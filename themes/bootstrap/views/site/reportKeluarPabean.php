<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
//echo "asdf";
?>

<style>
    .search-table-outter {border:2px solid #005580;}
.search-table{table-layout: auto; margin:0px auto 0px auto; }
.search-table, td, th{border-collapse:collapse; border:1px solid #777;}
th{padding:20px 7px; font-size:15px; color:#444; background:#f0f0f0;}
//td{padding:5px 10px; height:35px;}

.search-table-outter { overflow-x: scroll;  overflow-y: hidden;  }
th, td { min-width: 130px; }
</style>

<h1>Laporan Pengeluaran Barang Per Pabean</h1>
<br>

<form action="<?php echo Yii::app()->createUrl('site/reportKeluarPabean'); ?>" method="post">
    <tr><td>Dari Tanggal:</td> <td><input type="date" name="date1" value="<?php echo $date1; ?>" /><td>&nbsp;&nbsp;&nbsp;</td><td>Sampai :&nbsp;</td><td><input type="date" name="date2" value="<?php echo $date2; ?>" /></td></tr>


    <input type="submit" value="Load" />


</form>

<div class="search-table-outter wrapper">
<table border="1" class="search-table inner table table-striped">
    <thead>
        <tr>
            <th rowspan="2" style="width:10;"> No.</th>
             <th rowspan="2">Jenis Dokumen</th>
            <th colspan="2">Dokumen Pabean</th>
        
            <th colspan="2">Bukti/Dokumen Pengeluaran</th>
        
             <th rowspan="2">Pemasok/Pengirim</th>
            <th rowspan="2">Kode Barang</th>
        <th rowspan="2">Nama Barang</th>
        <th rowspan="2">Jumlah</th>
        <th rowspan="2">Satuan</th>
        <th rowspan="2">Nilai (IDR)</th>   
        </tr>
        
        <tr>
        
        <th>Nomor</th>
        <th>Tanggal <br>(Y-M-D)</th>
        <th>Nomor</th>
        <th>Tanggal <br>(Y-M-D)</th>
        
        </tr>
    </thead>
   <?php 
        $i=1;
        foreach($model->getData() as $row){ ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row["dp_jenis"];?></td>
        <td><?php echo $row["dp_nomor"];?></td>
        <td><?php   $dp=date('d F Y',  strtotime($row["dp_tanggal"]));
                    echo$dp;?></td>
        <td><?php echo $row["bpb_nomor"];?></td>
        <td><?php   $dp=date('d F Y',  strtotime($row["bpb_tanggal"]));
                    echo$dp;?></td>
        <td><?php echo $row["pengirim"];?></td>
        <td><?php echo $row["kode_barang"];?></td>
        <td><?php echo $row["nama_barang"];?></td>
        <td style="text-align: right;"><?php echo $row["jumlah"];?></td>
        <td><?php echo $row["satuan"];?></td>
        <td style="text-align: right;"><?php echo number_format($row["nilai"],2,",","."); ?></td> 
    </tr>
    <?php
    $i++;
    
        } ?>
</table>
</div>
<br>
<br>



<?php
/* $this->widget('bootstrap.widgets.TbGridView',array(
  'id'=>'masuk-header-grid',
  'dataProvider'=>$model,
  //'filter'=>$model,
  'columns'=>array(
  'id_masuk_header',
  'dp_jenis',
  'dp_nomor',
  'dp_tanggal',
  'bpb_nomor',
  'bpb_tanggal',


  /*
  'pengirim',
 */
//array(
//	'class'=>'bootstrap.widgets.TbButtonColumn',
//),
//	),
//)); 
?>




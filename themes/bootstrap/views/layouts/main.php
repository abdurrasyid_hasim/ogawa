<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
    

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        
	<?php Yii::app()->bootstrap->register(); ?>
        
        <style>
                .navbar-inner {
            background-color: #006699;
            background-image: none; 
            
        }
        
        .navbar .nav > li > a{
            
            color: #ffffff;
            text-decoration: false;
            text-shadow: none;
        }
        
        .navbar .nav > li > a:focus, .navbar .nav > li > a:hover {
            color: #FFD324;
            text-decoration: none;
            background-color: transparent;
        }
        
        .navbar .nav li.dropdown > .dropdown-toggle .caret {
            border-top-color: #ffffff;
            border-bottom-color: none;
            } 
        
        .navbar .brand {
            
            color: #ffffff;
            text-shadow: none;
        }
        //.nav-collapse .nav>li>a, .nav-collapse .dropdown-menu a {
        //    color: #e4e4e4;
        //}
        
        #footerfixed{
   position:fixed;
   left:0px;
   padding: 4px;
   bottom:0px;
   height:15px;
   width:100%;
   //font-size: 8;
   background:#e4e4e4;
}
    </style>
</head>

<body>

<?php

/*$this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
                array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
));*/

$this->widget(
    'bootstrap.widgets.TbNavbar',
    array(
        //'type' => 'inverse',
        'brand' =>Yii::app()->name,
        'brandUrl' => Yii::app()->baseUrl,
        'collapse' => true, // requires bootstrap-responsive.css
        //'fixed' => false,
        //'fluid' => true,
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'type' => 'navbar',
                'items' => array(
                    array(
                        'label' => 'Report',
                        'url' => '#',
                        'visible'=>Yii::app()->user->getRole()<=2,
                        'items' => array(
                            array('label' => 'Laporan Pemasukan Barang Per Dokumen Pabean', 'url' =>array('masukheader/reportMasukPabean')),
                            array('label' => 'Laporan Pengeluaran Barang Per Dokumen Pabean', 'url' => array('keluarheader/reportKeluarPabean')),
                            array('label' => 'Laporan Posisi WIP',  'url' => array('inventorymaster/reportWIP')),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Bahan Baku/Bahan Penolong', 'url' => array('tranheader/reportMutasiRM')),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Barang Jadi', 'url' => array('tranheader/reportMutasiFG')),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Mesin dan Peralatan', 'url' => array('tranheader/reportMutasiMP')),
                            array('label' => 'Laporan Pertanggungjawaban Barang Reject dan Scrap', 'url' => array('tranheader/reportMutasiSR')),
                        )
                    ),
                    array('label' => 'Log', 'url' => array('log/admin'),'visible'=>Yii::app()->user->getRole()==2,),
                    array(
                        'label' => 'Transaksi',
                        'url' => '#',
                        'visible'=>Yii::app()->user->getRole()<=1,
                        'items' => array(
                            //array('label'=>'Transaksi Baru', 'url'=>array('Tranheader/createNewTransaction')),
                           // '---',
                            array('label' => 'Mutasi Transaksi', 'url' =>array('Tranheader/admin')),
                             array('label' => 'Impor Pemasukan Barang Per Dokumen Pabean', 'url' =>array('masukheader/Tampilxml')),
                            array('label' => 'Impor Pengeluaran Barang Per Dokumen Pabean', 'url' =>array('keluarheader/importxml')),
                           /* array('label' => 'Pemasukan', 'url' => '#'),
                            array('label' => 'Pengeluaran', 'url' => '#'),
                            array('label' => 'Penyesuaian', 'url' => '#'),
                            array('label' => 'Stock Opanme', 'url' => '#'),*/
                        )
                    ),


                    array(
                        'label' => 'Maintenance',
                        'url' => '#',
                        'visible'=>Yii::app()->user->getRole()<=1,
                        'items' => array(
                            array('label' => 'Inventory Master', 'url'=>array('/inventorymaster/admin')),
                        )
                    ),

                    array(
                        'label' => 'User Management',
                        'url' => '#',
                        'visible'=>Yii::app()->user->getRole()<=1,
                        'items' => array(
                            array('label' => 'user', 'url' => array('/user/admin')),
                            array('label' => 'log viewer', 'url' =>array('/log/admin')),
                        )
                    ),


                ),
            ),
           // '<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div></form>',
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'type' => 'navbar',
                'htmlOptions' => array('class' => 'pull-right'),
                'items' => array(
                    array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)

                ),
            ),
        ),
    )
);
  ?>
 

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>
                

	<div class="clear"></div>
        
	<div class="pagination-centered" id="footerfixed">
    
            <div class="" style="font-size: 11px; padding-top: 0px; padding-bottom: 2px; color: #666666">
            
            <?php //echo date('Y'); ?> Powered by <a href="http://vertipolar.com/" target="_blank" style="color: #666666;">VertipolarBC</a><br/>
            <?php //echo Yii::powered(); ?>
        </div>
	</div>  <!-- footer -->

</div><!-- page -->

</body>
</html>

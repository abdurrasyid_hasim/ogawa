<?php
$this->breadcrumbs=array(
	'Report Pemasukan Beas'=>array('index'),
	$model->lap_masuk_id,
);

$this->menu=array(
	array('label'=>'List ReportPemasukanBea','url'=>array('index')),
	array('label'=>'Create ReportPemasukanBea','url'=>array('create')),
	array('label'=>'Update ReportPemasukanBea','url'=>array('update','id'=>$model->lap_masuk_id)),
	array('label'=>'Delete ReportPemasukanBea','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->lap_masuk_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ReportPemasukanBea','url'=>array('admin')),
);
?>

<h1>View ReportPemasukanBea #<?php echo $model->lap_masuk_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'lap_masuk_id',
		'dp_jenis',
		'dp_no',
		'dp_tanggal',
		'bpb_no',
		'bpb_tanggal',
		'pengirim_barang',
		'kode_barang',
		'nama_barang',
		'jumlah',
		'satuan',
		'nilai',
		'id_user',
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Report Pemasukan Beas'=>array('index'),
	$model->lap_masuk_id=>array('view','id'=>$model->lap_masuk_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ReportPemasukanBea','url'=>array('index')),
	array('label'=>'Create ReportPemasukanBea','url'=>array('create')),
	array('label'=>'View ReportPemasukanBea','url'=>array('view','id'=>$model->lap_masuk_id)),
	array('label'=>'Manage ReportPemasukanBea','url'=>array('admin')),
);
?>

<h1>Update ReportPemasukanBea <?php echo $model->lap_masuk_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
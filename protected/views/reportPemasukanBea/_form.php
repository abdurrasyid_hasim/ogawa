<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'report-pemasukan-bea-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'lap_masuk_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'dp_jenis',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'dp_no',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'dp_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bpb_no',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bpb_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'pengirim_barang',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'kode_barang',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'nama_barang',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'jumlah',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'satuan',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nilai',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_user',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

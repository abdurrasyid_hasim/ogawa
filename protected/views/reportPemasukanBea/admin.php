<?php
$this->breadcrumbs=array(
	'Report Pemasukan Beas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ReportPemasukanBea','url'=>array('index')),
	array('label'=>'Create ReportPemasukanBea','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('report-pemasukan-bea-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Report Pemasukan Beas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'report-pemasukan-bea-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'lap_masuk_id',
		'dp_jenis',
		'dp_no',
		'dp_tanggal',
		'bpb_no',
		'bpb_tanggal',
		/*
		'pengirim_barang',
		'kode_barang',
		'nama_barang',
		'jumlah',
		'satuan',
		'nilai',
		'id_user',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>

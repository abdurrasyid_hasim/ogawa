<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('lap_masuk_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->lap_masuk_id),array('view','id'=>$data->lap_masuk_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_jenis')); ?>:</b>
	<?php echo CHtml::encode($data->dp_jenis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_no')); ?>:</b>
	<?php echo CHtml::encode($data->dp_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->dp_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpb_no')); ?>:</b>
	<?php echo CHtml::encode($data->bpb_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpb_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->bpb_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengirim_barang')); ?>:</b>
	<?php echo CHtml::encode($data->pengirim_barang); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_barang')); ?>:</b>
	<?php echo CHtml::encode($data->kode_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_barang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuan')); ?>:</b>
	<?php echo CHtml::encode($data->satuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai')); ?>:</b>
	<?php echo CHtml::encode($data->nilai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user')); ?>:</b>
	<?php echo CHtml::encode($data->id_user); ?>
	<br />

	*/ ?>

</div>
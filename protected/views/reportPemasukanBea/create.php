<?php
$this->breadcrumbs=array(
	'Report Pemasukan Beas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ReportPemasukanBea','url'=>array('index')),
	array('label'=>'Manage ReportPemasukanBea','url'=>array('admin')),
);
?>

<h1>Create ReportPemasukanBea</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php

               /* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		));*/

                $this->widget(
    'booster.widgets.TbNavbar',
    array(
        'type' => 'inverse',
        'brand' => 'Project name',
        'brandUrl' => '#',
        'collapse' => true, // requires bootstrap-responsive.css
        'fixed' => false,
        'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'items' => array(
                    array(
                        'label' => 'Report',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Laporan Pemasukan Barang Per Pabean', 'url' => '#'),
                            array('label' => 'Laporan Pengeluaran Barang Per Dokumen Pabean', 'url' => '#'),
                            array('label' => 'Laporan Posisi WIP', 'url' => '#'),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Bahan Baku/Bahan Penolong', 'url' => '#'),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Barang Jadi', 'url' => '#'),
                            array('label' => 'Laporan Pertanggungjawaban Mutasi Mesin dan Peralatan', 'url' => '#'),
                            array('label' => 'Laporan Pertanggungjawaban Barang Reject dan Scrap', 'url' => '#'),
                        )
                    ),
                    array(
                        'label' => 'Transaksi',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Saldo Awal', 'url' => '#'),
                            array('label' => 'Pemasukan', 'url' => '#'),
                            array('label' => 'Pengeluaran', 'url' => '#'),
                            array('label' => 'Penyesuaian', 'url' => '#'),
                            array('label' => 'Stock Opanme', 'url' => '#'),
                        )
                    ),


                    array(
                        'label' => 'Maintenance',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Inventory Master', 'url' => '#'),
                        )
                    ),

                    array(
                        'label' => 'User Management',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'user', 'url' => '#'),
                            array('label' => 'log viewer', 'url' => '#'),
                        )
                    ),


                ),
            ),
           // '<form class="navbar-form navbar-left" action=""><div class="form-group"><input type="text" class="form-control" placeholder="Search"></div></form>',
            array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'htmlOptions' => array('class' => 'pull-right'),
                'items' => array(
                    array(
                        'label' => 'Login',
                        'url' => '#',
                        'items' => array(
                            array('label' => 'Action', 'url' => '#'),
                            array('label' => 'Another action', 'url' => '#'),
                            array(
                                'label' => 'Something else here',
                                'url' => '#'
                            ),
                            '---',
                            array('label' => 'Separated link', 'url' => '#'),
                        )
                    ),
                ),
            ),
        ),
    )
);
           
                ?>


	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

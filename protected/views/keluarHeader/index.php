<?php
$this->breadcrumbs=array(
	'Keluar Headers',
);

$this->menu=array(
	array('label'=>'Create KeluarHeader','url'=>array('create')),
	array('label'=>'Manage KeluarHeader','url'=>array('admin')),
);
?>

<h1>Keluar Headers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

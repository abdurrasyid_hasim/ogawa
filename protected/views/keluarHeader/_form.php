<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'keluar-header-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'dp_jenis',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'dp_nomor',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'dp_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bpb_nomor',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'bpb_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pengirim',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id_keluar_header',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'dp_jenis',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'dp_nomor',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'dp_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bpb_nomor',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'bpb_tanggal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pengirim',array('class'=>'span5','maxlength'=>100)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

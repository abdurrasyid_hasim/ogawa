<?php
$this->breadcrumbs=array(
	'Keluar Headers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KeluarHeader','url'=>array('index')),
	array('label'=>'Manage KeluarHeader','url'=>array('admin')),
);
?>

<h1>Create KeluarHeader</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
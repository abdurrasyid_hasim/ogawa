<?php
$this->breadcrumbs=array(
	'Keluar Headers'=>array('index'),
	$model->id_keluar_header=>array('view','id'=>$model->id_keluar_header),
	'Update',
);

$this->menu=array(
	array('label'=>'List KeluarHeader','url'=>array('index')),
	array('label'=>'Create KeluarHeader','url'=>array('create')),
	array('label'=>'View KeluarHeader','url'=>array('view','id'=>$model->id_keluar_header)),
	array('label'=>'Manage KeluarHeader','url'=>array('admin')),
);
?>

<h1>Update KeluarHeader <?php echo $model->id_keluar_header; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
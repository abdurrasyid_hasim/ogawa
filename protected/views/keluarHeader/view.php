<?php
$this->breadcrumbs=array(
	'Keluar Headers'=>array('index'),
	$model->id_keluar_header,
);

$this->menu=array(
	array('label'=>'List KeluarHeader','url'=>array('index')),
	array('label'=>'Create KeluarHeader','url'=>array('create')),
	array('label'=>'Update KeluarHeader','url'=>array('update','id'=>$model->id_keluar_header)),
	array('label'=>'Delete KeluarHeader','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_keluar_header),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KeluarHeader','url'=>array('admin')),
);
?>

<h1>View KeluarHeader #<?php echo $model->id_keluar_header; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id_keluar_header',
		'dp_jenis',
		'dp_nomor',
		'dp_tanggal',
		'bpb_nomor',
		'bpb_tanggal',
		'pengirim',
	),
)); ?>

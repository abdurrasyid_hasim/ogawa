<?php
$this->breadcrumbs=array(
	'Daftar Transaksi',
	'Create',
);

$this->menu=array(
	//array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Manage Transaksi','url'=>array('admin')),
);
?>

<h1>Transaksi Baru</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
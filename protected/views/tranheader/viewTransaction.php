<?php
$this->menu=array(
	//array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Transaksi Baru','url'=>array('transaction')),
        array('label'=>'Update Transaksi', 'url'=>array('')),
);
?>

<h1>View Tranheader #<?php //echo $model->tranheader_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$tranheader,
	'attributes'=>array(
		'tranheader_id',
		'tanggal_tran',
		'type_tran',
		'status_tran',
		'user_id',
                'user.username',
               /* array(
                    'name'=>'user_id',
                    'value'=>'$data->user->user_name',
                ),*/
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'trandetail-grid',
	'dataProvider'=>$trandetail,
	//'filter'=>$trandetail,
	'columns'=>array(
		//'trandetail_id',
		'kode_barang',
		'nama_barang',
		'quantity',
		'satuan',
		'tranheader_id',
		//array(
		//	'class'=>'bootstrap.widgets.TbButtonColumn',
		//),
	),
)); ?>

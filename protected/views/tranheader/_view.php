<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('tranheader_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->tranheader_id),array('view','id'=>$data->tranheader_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_tran')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_tran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type_tran')); ?>:</b>
	<?php echo CHtml::encode($data->type_tran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_tran')); ?>:</b>
	<?php echo CHtml::encode($data->status_tran); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />


</div>
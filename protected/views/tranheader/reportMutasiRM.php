<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>
<style>
  //  .search-table-outter {border:2px solid #005580;}
//.search-table{table-layout: auto; margin:0px auto 0px auto; }
//.search-table, td, th{border-collapse:collapse; border:1px solid #777;}
//th{padding:20px 7px; font-size:15px; color:#444; background:#f0f0f0;}
//td{padding:5px 10px; height:35px;}

//.search-table-outter { overflow-x: scroll;  overflow-y: hidden;  }
//th, td { min-width: 130px; }
.table td{
    font-size: 8pt;
}
</style>
<script>
 function submitFunction(i) {
    if (i==1) {
         document.theForm.target="";
     document.theForm.action= "<?php echo Yii::app()->createUrl('tranheader/reportMutasiRM'); ?>";}
   if (i==2) {
       document.theForm.action="<?php echo Yii::app()->createUrl('tranheader/mutasiRmPDF'); ?>";
        document.theForm.target="_blank";}
   document.theForm.submit()
   }
</script>
<h1>Laporan Mutasi Bahan Baku/Penolong</h1>
<br>
<form name="theForm" method="post" target="">
  <tr><td>Dari Tanggal:
      <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date1',
     'value'=>date('d-m-Y', strtotime($date1)),
     'id'=>'date1',
    // additional javascript options for the date picker plugin
    'options'=>array(
        
//'showAnim' => 'fold',
'dateFormat' => 'dd-mm-yy', // save to db format
//'altField' => 'date1',
'altFormat' => 'yy-mm-dd', // show to user format
),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
  //      'id'=>'date1',
    ),
));
 ?>
      </td>
</td> 
    <td>&nbsp;&nbsp;</td><td>Sampai :</td><td>
                
     <?php 
     
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date2',
     'id'=>'date2',
     'value'=>date('d-m-Y', strtotime($date2)),
     
    // additional javascript options for the date picker plugin
    'options'=>array(
       // 'showAnim'=>'fold',
        'dateFormat'=>'dd-mm-yy',
       'altFormat'=>'yy-mm-dd',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    //    'id'=>'date2',
    ),
));
 ?>
            </td></tr>
        <input class="btn btn-primary" name="submit2" type="submit" value="Load" onclick="submitFunction(1)" />
        <input class="btn btn-primary" name="submit" type="submit" value="Eksport" onclick="submitFunction(2)"/>
    <td>&nbsp;</td></tr>
</form>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'RESET',
    'type'=>'warning', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'mini', // null, 'large', 'small' or 'mini'
    'url'=>array('tranheader/resetRM'),
    
)); ?>
       

<?php  $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'masuk-header-grid',
	'dataProvider'=>$model,
	//'filter'=>$model,
        'type'=>'striped',
        //'template'=>"{items}\n{pager}",
        'htmlOptions'=>array('style'=>'text-size:8pt;'),
	'columns'=>array(
            array('header'=>'No.',
          'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)
',
            ),
            'code',
            'description',
            'satuan',
            array(
                'name'=>'SaldoAwal',
                'value'=>'number_format($data->SaldoAwal,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'Pemasukan',
            array(
                'name'=>'Pemasukan',
                'value'=>'number_format($data->Pemasukan,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'Pengeluaran',
            array(
                'name'=>'Pengeluaran',
                'value'=>'number_format($data->Pengeluaran,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'Penyesuain',
            array(
                'name'=>'Penyesuain',
                'value'=>'number_format($data->Penyesuain,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'SaldoBuku',
            array(
                'name'=>'SaldoBuku',
                'value'=>'number_format($data->SaldoBuku,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'StockOpname',
            array(
                'name'=>'StockOpname',
                'value'=>'number_format($data->StockOpname,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
            //'Selisih',
            array(
                'name'=>'Selisih',
                'value'=>'number_format($data->Selisih,4,",",".")',
                'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
)));  ?>



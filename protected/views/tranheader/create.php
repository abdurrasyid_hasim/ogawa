<?php
$this->breadcrumbs=array(
	'Transaksi/',
	'Transaksi Baru',
);

$this->menu=array(
	//array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Daftar Transaksi','url'=>array('admin')),
);
?>

<h1>Transaksi Baru</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
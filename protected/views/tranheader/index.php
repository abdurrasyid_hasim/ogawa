<?php
$this->breadcrumbs=array(
	'Tranheaders',
);

$this->menu=array(
	array('label'=>'Create Tranheader','url'=>array('create')),
	array('label'=>'Manage Tranheader','url'=>array('admin')),
);
?>

<h1>Tranheaders</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

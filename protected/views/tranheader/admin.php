<?php
$this->breadcrumbs=array(
	'Mutasi Transaksi',
	//'Manage',
);

$this->menu=array(
	//array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Transaksi Baru','url'=>array('createNewTransaction')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tranheader-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Mutasi Transaksi</h1>


<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php  //$this->renderPartial('_search',array(
//	'model'=>$model,
//)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'tranheader-grid',
	'dataProvider'=>$model->search(),
        //'sort'=>'tranheader_id DESC',
	'filter'=>$model,
        'type'=>'striped',
	'columns'=>array(
            array(
		'name'=>'tranheader_id',
                'value'=>'$data->tranheader_id',
                'htmlOptions'=>array('width'=>'40'),
                ),
            
		'tanggal_tran',
		
            array(
            'name'=>'type_tran',
                //'value'=>'$data->type_tran',
            'type'=>'raw',
            'value'=>function($data){
                        if ($data->type_tran=='P'){
                            $class = 'Pemasukan';
                        }
                        else if ($data->type_tran=='K'){
                            $class = 'Pengeluaran';
                            //$class=$data->status_tran;
                        }
                        else if ($data->type_tran  == 'S'){
                            $class = 'Penyesuaian';
                        }
                        else if ($data->type_tran  == 'O'){
                            $class = 'Stok Opname';
                        }
                        else if ($data->type_tran == 'A'){
                            $class = 'Saldo Awal';
                        }
                        return $class;
                    },
            'filter'=>array(''=>'All','P'=>'Pemasukan (P)','K'=>'Pengeluaran (K)','S'=>'Penyesuaian (S)','O'=>'Stok Opname (O)'/*, 'A'=>'Saldo Awal (A)'*/),
            ),
            
		array(
            'name'=>'status_tran',
            'value'=>function($data){
                        if ($data->status_tran  == 'P'){
                            $stat = 'Processed';
                        }
                        else{
                            $stat = 'New';
                        }
                        return $stat;
                    },
            'filter'=>array(''=>'All','P'=>'Processed (P)','N'=>'New (N)'),
            /*'htmlOptions'=>function($data){
                        if ($data->status_tran  == 'P'){
                            $class = "'class'=>'label label-inverse'";
                        }
                        else{
                            $class = "'class'=>'label label-success'";
                        }
                        return $class;
                    },*/
                            
            ),
                            'keterangan',
		//'user_id',
                array(            
                    'name'=>'user_id',
                    'type'=>'raw',
                    'value'=>'$data->user->user_name',
                ),
		/*array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),*/
            array(
                   //'name'=>'View',
                   'header'=>'Lihat',
                   'type'=>'raw',
                   'value'=>'CHtml::link("Lihat",Yii::app()->createUrl("trandetail/transactionAdd", array("id"=>$data["tranheader_id"])))',
                   'htmlOptions'=>array('width'=>'40'),
                        
           ),
           array(
                   //'name'=>'View',
                   'header'=>'Hapus',
                   'type'=>'raw',
                   'value'=>function($data){
                        if ($data->status_tran  == 'P'){
                            $class = CHtml::link("Hapus","#", array("style"=>'pointer-events: none; color:gray;', "confirm" => "Are yous sure?"));
                        }
                        else{
                            $class = CHtml::link("Hapus","#", array("submit"=>array("delete", "id"=>$data["tranheader_id"]), "confirm" => "Are you sure?"));
                        }
                        return $class;
                        /*'CHtml::link("Hapus","#", array("submit"=>array("delete", "id"=>$data["tranheader_id"]), "confirm" => "Are yous sure?"))',*/
                   },
                    'htmlOptions'=>array('width'=>'40'),
                        
           ),
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Tranheaders'=>array('index'),
	$model->tranheader_id,
);

$this->menu=array(
	array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Create Tranheader','url'=>array('create')),
	array('label'=>'Update Tranheader','url'=>array('update','id'=>$model->tranheader_id)),
	array('label'=>'Delete Tranheader','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->tranheader_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tranheader','url'=>array('admin')),
);
?>

<h1>Transaksi #<?php echo $model->tranheader_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'tranheader_id',
		'tanggal_tran',
		'type_tran',
		'status_tran',
		'user_id',
	),
)); ?>

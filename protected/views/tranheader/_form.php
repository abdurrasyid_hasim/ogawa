<script type="text/javascript">
function enable()
{
    document.getElementById("Tranheader_status_tran").disabled=false;
}

</script>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tranheader-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('onsubmit'=>'enable()'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->textFieldRow($model,'tanggal_tran',array('class'=>'span5')); ?>
        
        <label for="Tranheader_type_tran" class="required">Tanggal tran <span class="required">*</span></label>
        <input class="span3" name="Tranheader[tanggal_tran]" id="Tranheader_tanggal_tran" type="date">
        
	<?php //echo $form->dropDownListRow($model,'type_tran',array('class'=>'span2','maxlength'=>1)); ?>
        
        <label for="Tranheader_type_tran" class="required">Type Tran <span class="required">*</span></label>
        <select name="Tranheader[type_tran]" id="Tranheader_type_tran">
                                        <option value="">-- Pilih Tipe Transaksi --</option>
                			<option value="P">Pemasukan</option>
                			<option value="K">Pengeluaran</option>
                			<option value="S">Penyesuaian</option>
                			<option value="O">Stok Opname</option>
                                      <!--  <option value="A">Saldo Awal</option>-->
        </select>

	<?php //echo $form->dropDownListRow($model,'status_tran',array('class'=>'span2','maxlength'=>1)); ?>
        
        <label for="Tranheader_status_tran" class="required">Status <span class="required">*</span></label>
        <select id="Tranheader_status_tran" name="Tranheader[status_tran]" id="Tranheader_status_tran" disabled="disable">
                                        <option value="N">New</option>
                			<option value="P">Processed</option>
        </select>
        
        <label for="Tranheader_keterangan">Keterangan</label>
        <textarea class="span3" name="Tranheader[keterangan]" id="Tranheader_keterangan"></textarea>
        
	<?php //echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
$this->breadcrumbs=array(
	'Tranheaders'=>array('index'),
	$model->tranheader_id=>array('view','id'=>$model->tranheader_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Create Tranheader','url'=>array('create')),
	array('label'=>'View Tranheader','url'=>array('view','id'=>$model->tranheader_id)),
	array('label'=>'Manage Tranheader','url'=>array('admin')),
);
?>

<h1>Update Tranheader <?php echo $model->tranheader_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
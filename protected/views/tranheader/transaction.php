<h3>Transaksi</h3>
<?php
//echo "Transaksi";
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript">
	var globalInt=1;
    $(document).ready(function(){
        
       // add new variabel  
       $("#add_variable").click(function(){
           
          var $varName = $("#variable_name").val();
          
          if($varName == "")
          {
             alert("Nama variabel harus diisi!");
             return;
          }
          
          $status = false;
          
          $("#input_container .input").each(function(){
             $name = $.trim($(this).find(".input_title_text").text());
             //console.log($name);
             
             if($name == $varName)
             {
                 alert("Nama variabel sudah ada!");
                 $status = true;
             }
          });
          
          if($status == true)
          {
              return;
          }
          
          $varNewInput = $("#template_var .input").clone();
          $varNewInput.find(".mf:gt(0)").remove();
          $varNewInput.find(".input_title_text").html("<span class=\"icon_var_input2\"></span> " + $varName);
          $varNewInput.find("#name").attr("name", $varName+"[name][]");
          $varNewInput.find("#param").attr("name", $varName+"[param][]");
          $varNewInput.find("#type").attr("name", $varName+"[type][]");
        
          $varNewInput.show().appendTo("#input_container");
          
          $("#variable_name").attr("value", "");
       });
    });
    
    function removeMf(obj)
    {
    	globalInt--;
        $(obj).parentsUntil(".var_kiri").remove();
    }
    
    function addMf(obj)
    {
        $varName = $.trim($(obj).parentsUntil("#input_container").find(".input_title_text").text());
        
        $buttonRemove = $(obj).next();
        $varNewInput = $("#template_mf .mf").clone();
        
        globalInt++;
        
        $varNewInput.find("#no").attr("no", "no[]");
        $varNewInput.find("#no").attr("value", globalInt);      
	$varNewInput.find("#kodebarang").attr("name", "Trandetail[kode_barang][]");
        $varNewInput.find("#namabarang").attr("name", "Trandetail[nama_barang][]");
        $varNewInput.find("#quantity").attr("name", "Trandetail[quantity][]");
        $varNewInput.find("#satuan").attr("name", "Trandetail[satuan][]");
        $varNewInput.find("#quantity").attr("value", 1); 
        
        var kode="'"+globalInt+"kodebarang'";
        var barang="'"+globalInt+"namabarang'";
        var satuan="'"+globalInt+"satuan'";
        $varNewInput.find("#kodebarang").attr("onchange", 'samain('+kode+','+barang+','+satuan+')');
        $varNewInput.find("#namabarang").attr("onchange", 'samain('+barang+','+kode+','+satuan+')');
       
        $varNewInput.find("#kodebarang").attr("id", globalInt+"kodebarang");
        $varNewInput.find("#namabarang").attr("id", globalInt+"namabarang");
        $varNewInput.find("#satuan").attr("id", globalInt+"satuan");
        
        //document.getElementById('satuan').disabled=false;
        
        
        $(obj).parentsUntil(".input").append($varNewInput);
        $(obj).parentsUntil(".input").append(obj);
        $(obj).parentsUntil(".input").append($buttonRemove);
        
    }
    
    function removeVar(obj)
    {
        $(obj).parentsUntil("#input_container").remove();
    }
    
    function samain(idElement,idElement2,idElement3,hidden)
    {
    var element=document.getElementById(idElement);
    var val=element.selectedIndex;
    document.getElementById(idElement2).selectedIndex=val; 
    document.getElementById(idElement3).selectedIndex=val;
    //document.getElementById(hidden).value=document.getElementById(idElement3).value;
    }
    
    function enable()
    {
        document.getElementById("satuan").disabled=false;
        document.getElementById("status").disabled=false;

        
        
        var i;
        for (i = 2; i<=globalInt; i++)
        {
        document.getElementById(i+"satuan").disabled=false;
        }
    }
       
</script>

<!-- <div class="input">
        <div class="input_title_text">
            <span class="icon_var_input"></span> Add Variabel
        </div>
        
        <input type="text" id="variable_name" value="" size="30"/>
        <input type="button" id="add_variable" value="Add"/>
    </div> -->
    
    <form action="<?php echo Yii::app()->createUrl('tranheader/transaction'); ?>" method="post" onsubmit="enable()">
    <div id="input_container">
        <?php //foreach($vars as $var): ?>
        
        <div class="input">
            <div class="input_title_text">
                <span class="icon_var_input2"></span> 
            </div>

            <div class="var_kiri">
                <?php //foreach($var->mfs as $mf): ?>
                <table class="mf" style="margin: 0px 0px 0px 0px;" border="0">
                	<tr>
                        <td>No. Transaksi</td>
                        <td><input type="text" name="Tranheader[notransaksi]" disabled="disabled" value=""/></td>
                        <td></td>
                        <td>Type</td>
                        <td>
                        	<select name="Tranheader[type_tran]" id="type">
                			<option>A</option>
                			<option>P</option>
                			<option>K</option>
                			<option>S</option>
                			<option>O</option>
            				</select>
                        </td>
                        </tr>
                        <tr>
                        <td>Tanggal</td>
                        <td><input type="date" name="Tranheader[tanggal_tran]" value="<?php echo date('Y-m-d'); ?>"/>
                                                <td></td>

                        <td>Status</td>
                        <td><select disabled="disable" name="Tranheader[status_tran]" id="status">
                			<option value="N">New</option>
                			<option value="P">Processed</option>
            				</select></td>
                        
                        
                    </tr>
                    <!--trandetail-->
                    
                    <tr height=80px><td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td syle="align=top;"><input type="submit" onsubmit="enable()" value="Save" /></td>
                        <td></td></tr>
                	<tr>
                        <td width="80px">No</td>
                        <td>Kode Barang</td>
                        <td>Nama Barang</td>
                        <td>Quantity</td>
                        <td>Satuan</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><input type="text" id="no" name="Trandetail[no][]" disabled="disabled" value="1"/></td>
                        <!--<td><input type="text" id="kodebarang" name="Trandetail[kode_barang][]"/></td>
                        <td><input type="text" id="namabarang" name="Trandetail[nama_barang][]"/></td>-->
                        <td><select id="kodebarang" onchange="samain('kodebarang','namabarang','satuan','satuanHidden')" name="Trandetail[kode_barang][]"/>
                            <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>"><?php echo $ii['code'];?></option>
                    
                            <?php } // foreach?>
                            </select></td>
                        <td><select id="namabarang" onchange="samain('namabarang','kodebarang','satuan','satuanHidden')" name="Trandetail[nama_barang][]"/>
                    <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>"><?php echo $ii['inventory'];?></option>
                    
                            <?php } // foreach?>
                            </select>
                </td>
                        <td><input type="text" id="quantity" name="Trandetail[quantity][]" value="1"/></td>
                        <!--<td><input type="text" id="satuan" name="Trandetail[satuan][]"/></td>-->
                        <td><select id="satuan"  disabled="disable" onsubmit="enable()" onchange="" name="Trandetail[satuan][]"/>
                            <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>" value="<?php echo $ii['satuan'];?>"><?php echo $ii['satuan'];?></option>
                    
                            <?php } // foreach?>
                            </select>
                            <input type="hidden" name="Trandetail[satuanHidden][]" id="satuanHidden" /></td>
                        <!-- <td><input type="button" id="delete_variable" value="X" onclick="removeMf(this)"></td> -->
                    </tr>
                </table> 
                <?php //endforeach; ?>
                <input type="button" id="add_mf" value="+" onclick="addMf(this)">
                <!-- <input type="button" id="delete_variable" value="Remove Variabel" onclick="removeVar(this)"> -->
            </div>
        </div>

        <?php //endforeach; ?>
    </div>
    <!--<input type="submit" value="Save" name="yt0" />-->
    </form>
</div>


<!--template input-->
<div id="template_mf" style="display: none;">
    
<table class="mf" style="margin: 0px 0px 0px 0px;">
    				<tr>
                          <td><input type="text" id="no" name="Trandetail[no][]" disabled="disabled" value=""/></td>
                        <!--<td><input type="text" id="kodebarang" name="Trandetail[kode_barang][]"/></td>
                        <td><input type="text" id="namabarang" name="Trandetail[nama_barang][]"/></td>-->
                        <td><select id="kodebarang" onchange="" name="Trandetail[kode_barang][]"/>
                            <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>"><?php echo $ii['code'];?></option>
                    
                            <?php } // foreach?>
                            </select></td>
                        <td><select id="namabarang" onchange="" name="Trandetail[nama_barang][]"/>
                    <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>"><?php echo $ii['inventory'];?></option>
                    
                            <?php } // foreach?>
                            </select>
                </td>
                        <td><input type="text" id="quantity" name="Trandetail[quantity][]"/></td>
                        <!--<td><input type="text" id="satuan" name="Trandetail[satuan][]"/></td>-->
                        <td><select id="satuan" disabled="disable"  onchange="" name="Trandetail[satuan][]"/>
                            <?php 
                            foreach($models->getData() as $i=>$ii) { ?>
                    <option id="<?php echo $ii['id_inventory'];?>"><?php echo $ii['satuan'];?></option>
                    
                            <?php } // foreach?>
                            </select>
                        <input type="hidden" name="Trandetail[satuanHidden][]" id="satuanHidden" /></td></td>
                        <td><input type="button" id="delete_variable" value="X" onclick="removeMf(this)"></td>  
                    </tr>
</table>
    
</div>
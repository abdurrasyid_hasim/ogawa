<?php
$this->breadcrumbs=array(
	'Inventory Master/',
	//'Manage',
);

$this->menu=array(
	//array('label'=>'List Inventorymaster','url'=>array('index')),
	array('label'=>'Inventory Baru','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventorymaster-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Inventory Master</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> 

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div> --> <!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'inventorymaster-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_inventory',
		'code',
		//'inventory',
		'description',
                array(
                    'name'=>'type',
                    'filter'=>array(''=>'All','F'=>'Finish Good','R'=>'Raw Material','W'=>'WIP','M'=>'Mesin dan Peralatan','S'=>'Scrapt dan Reject'),
                    'value'=>'$data->type',
                    ),
            
		array(
                    'name'=>'satuan',
                    'filter'=>array(''=>'All','PCS'=>'PCS', 'EA'=>'EA', 'PACK'=>'PACK', 'KG'=>'KG', 'LTR'=>'LTR', 'SET'=>'SET','UNIT'=>'UNIT','LOT'=>'LOT','BOX'=>'BOX','0'=>'0','MTR'=>'MTR','NOS'=>'NOS','M3'=>'M3','M2'=>'M2','TON'=>'TON','DRUM'=>'DRUM','SUM'=>'SUM','RROL'=>'RROL','CTN'=>'CTN'),
                    'value'=>'$data->satuan',
                    ),
                            
		array(
                    'name'=>'status',
                    'filter'=>array(''=>'All','A'=>'Aktif (A)','I'=>'Inakftif (I)'),
                    'value'=>function($data){
                        if ($data->status  == 'A'){
                            $class = 'Aktif';
                        }
                        else{
                            $class = 'Inaktif';
                        }
                        return $class;
                    }),
		/*
		'user_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>

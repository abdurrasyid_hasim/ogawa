<?php
$this->breadcrumbs=array(
	'Inventory Master/',
	$model->id_inventory,
);

$this->menu=array(
	//array('label'=>'List Inventorymaster','url'=>array('index')),
	array('label'=>'Inventory Baru','url'=>array('create')),
	array('label'=>'Update Inventory','url'=>array('update','id'=>$model->id_inventory)),
	array('label'=>'Hapus Inventory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_inventory),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Inventory Master','url'=>array('admin')),
);
?>

<h1>Lihat Inventory #<?php echo $model->code; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		//'id_inventory',
		'code',
		//'inventory',
		'description',
                'type',
		'satuan',
		'status',
		'user.user_name',
	),
)); ?>

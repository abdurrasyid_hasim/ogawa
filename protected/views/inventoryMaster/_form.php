<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'inventorymaster-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>30)); ?>

	<?php //echo $form->textFieldRow($model,'inventory',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'description',array('class'=>'span5','maxlength'=>300)); ?>

	<?php echo $form->dropDownListRow($model,'type',array('F'=>'Finish Good','R'=>'Raw Material','W'=>'WIP','M'=>'Mesin dan Peralatan','S'=>'Scrapt dan Reject'),array('empty'=>'-- Pilih Tipe Inventory --')); ?>
        
        <?php echo $form->dropDownListRow($model,'satuan',array('PCS'=>'PCS', 'EA'=>'EA', 'PACK'=>'PACK', 'KG'=>'KG', 'LTR'=>'LTR', 'SET'=>'SET','UNIT'=>'UNIT','LOT'=>'LOT','BOX'=>'BOX','0'=>'0','MTR'=>'MTR','NOS'=>'NOS','M3'=>'M3','M2'=>'M2','TON'=>'TON','DRUM'=>'DRUM','SUM'=>'SUM','RROL'=>'RROL','CTN'=>'CTN'),array('empty'=>'-- Pilih Satuan --')); ?>

	<?php echo $form->dropDownListRow($model, 'status', array('A'=>'Aktif', 'I'=>'Tidak Aktif')); ?>
        <?php //echo $form->textFieldRow($model,'status',array('class'=>'span5','maxlength'=>1)); ?>

	<?php //echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

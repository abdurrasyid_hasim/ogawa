<?php
$this->breadcrumbs=array(
	'Inventory Master/',
	$model->id_inventory=>array('view','id'=>$model->id_inventory),
	'Update',
);

$this->menu=array(
	//array('label'=>'List Inventorymaster','url'=>array('index')),
	array('label'=>'Inventory Baru','url'=>array('create')),
	//array('label'=>'Lihat Inventory','url'=>array('view','id'=>$model->id_inventory)),
	array('label'=>'Inventory Master','url'=>array('admin')),
);
?>

<h1>Update Inventory <?php echo $model->id_inventory; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
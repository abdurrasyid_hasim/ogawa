<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>
<h1>Laporan Posisi WIP</h1>
<br>
<script>
 function submitFunction(i) {
    if (i==1) {
        document.theForm.target="";
     document.theForm.action= "<?php echo Yii::app()->createUrl('inventoryMaster/reportWIP'); ?>";}
   if (i==2) {
       document.theForm.action="<?php echo Yii::app()->createUrl('inventoryMaster/laporanWipPDF'); ?>";
        document.theForm.target="_blank";
    }
   document.theForm.submit()
   }
</script>

<form name="theForm" method="post" target="">
       <tr><td>Dari Tanggal:
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date1',
     'value'=>date('d-m-Y', strtotime($date1)),
     'id'=>'date1',
    // additional javascript options for the date picker plugin
    'options'=>array(
        
//'showAnim' => 'fold',
'dateFormat' => 'dd-mm-yy', // save to db format
//'altField' => 'date1',
'altFormat' => 'yy-mm-dd', // show to user format
),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
  //      'id'=>'date1',
    ),
));
 ?>
            </td>
</td> 
        <td>&nbsp;&nbsp;</td><td>Sampai :</td><td>
                
     <?php 
     
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date2',
     'id'=>'date2',
     'value'=>date('d-m-Y', strtotime($date2)),
     
    // additional javascript options for the date picker plugin
    'options'=>array(
       // 'showAnim'=>'fold',
        'dateFormat'=>'dd-mm-yy',
       'altFormat'=>'yy-mm-dd',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    //    'id'=>'date2',
    ),
));
 ?>
            </td></tr>
        <input class="btn btn-primary" name="submit2" type="submit" value="Load" onclick="submitFunction(1)" />
        <input class="btn btn-primary" name="submit" type="submit" value="Eksport" onclick="submitFunction(2)"/>
    <td>&nbsp;</td>
</form>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'RESET',
    'type'=>'warning', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'mini', // null, 'large', 'small' or 'mini'
    'url'=>array('inventorymaster/resetWIPReport'),
    
)); ?>

<?php  $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'masuk-header-grid',
	'dataProvider'=>$model,
	//'filter'=>$model,
        'type'=>'striped',
        //'template'=>"{items}\n{pager}",
        'htmlOptions'=>array('style'=>'text-size:8pt;'),
	'columns'=>array(
            array(
            'name'=>'kode_barang',
            'header'=>'Kode Barang',
            'value'=>'$data->kode_barang',
                ),
            
            array(
            'name'=>'nama_barang',
            'header'=>'Nama Barang',
            'value'=>'$data->nama_barang',
                ),
            'Satuan',
            'Jumlah',  
	
)));  ?>




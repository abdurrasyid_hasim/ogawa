<?php
$this->breadcrumbs=array(
	'Inventory Master/',
	'Inventory Baru',
);

$this->menu=array(
	//array('label'=>'List Inventorymaster','url'=>array('index')),
	array('label'=>'Inventory Master','url'=>array('admin')),
);
?>

<h1>Inventory Baru</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
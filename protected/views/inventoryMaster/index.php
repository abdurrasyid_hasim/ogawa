<?php
$this->breadcrumbs=array(
	'Inventorymasters',
);

$this->menu=array(
	array('label'=>'Inventory Baru','url'=>array('create')),
	array('label'=>'Manage Inventory','url'=>array('admin')),
);
?>

<h1>Inventorymasters</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

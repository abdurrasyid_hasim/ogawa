
<?php
Yii::import('application.extensions.fpdf.*');

$model;$date1;$date2;
class laporanWipPDF extends fpdf
{
        //public $date1;
        //public $date2;
        private $data = array();
  	private $options = array(
  		'filename' => '',
  		'destinationfile' => '',
  		'paper_size'=>'A4',
  		'orientation'=>'P'
  	);

  	function __construct($data = array(), $options = array()) {
    	parent::__construct();
    	$this->data = $data;
    	$this->options = $options;
	}

        public function headerTable(){
            $h = 13;
		$left = 40;
		$top = 80;
		#tableheader
		$this->SetFillColor(225,225,225);
                $this->SetFont("", "", 11);
		$left = $this->GetX();
		$this->Cell(25,$h,'No',1,0,'L',true);
                $this->SetX($left += 25); $this->Cell(90, $h, 'Kode Barang ', 1, 0, 'C',true);
		$this->SetX($left += 90); $this->Cell(130, $h, 'Nama Barang', 1, 0, 'C',true);
		$this->SetX($left += 130); $this->Cell(70, $h, 'Satuan ', 1, 0, 'C',true);
		$this->SetX($left += 70); $this->Cell(80, $h, 'Jumlah', 1, 0, 'C',true);
                $this->SetX($left += 80); $this->Cell(120, $h, 'Keterangan ', 1, 0, 'C',true);
		$this->Ln(13);
        }


        public function rptDetailData($date1, $date2) {
		//
		$border = 0;
		$this->AddPage();

		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;

		//header
		$this->SetFont("", "B", 12);
                $this->SetX($left); $this->Cell(0, 10, 'Kawasan Berikat PT. OGAWA INDONESIA', 0, 1,'L');
		$this->Ln(5);
		$this->SetX($left); $this->Cell(0, 10, 'Laporan Posisi Barang Dalam Proses (WIP)', 0, 1,'L');
		$this->Ln(5);
                $this->SetX($left); $this->Cell(0, 10, 'Periode '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($date1)).' s/d '.Yii::app()->dateFormatter->format("d MMMM y",strtotime($date2)), 0, 1,'L');
		$this->Ln(10);

                $h = 13;
		$left = 40;
		$top = 80;
		#tableheader
		$this->SetFillColor(225,225,225);
                $this->SetFont("", "", 11);
		$left = $this->GetX();
		$this->Cell(25,$h,'No',1,0,'L',true);
                $this->SetX($left += 25); $this->Cell(90, $h, 'Kode Barang ', 1, 0, 'C',true);
		$this->SetX($left += 90); $this->Cell(130, $h, 'Nama Barang', 1, 0, 'C',true);
		$this->SetX($left += 130); $this->Cell(70, $h, 'Satuan ', 1, 0, 'C',true);
		$this->SetX($left += 70); $this->Cell(80, $h, 'Jumlah', 1, 0, 'C',true);
                $this->SetX($left += 80); $this->Cell(120, $h, 'Keterangan ', 1, 0, 'C',true);
		$this->Ln(13);

		$this->SetFont('Arial','',9);
		$this->SetWidths(array(25,90,130,70,80,120));
		$this->SetAligns(array('C','L','L','L','R','L'));
		$no = 1; $this->SetFillColor(255);
		foreach ($this->data as $baris) {
			$this->Row(
				array($no++,
				$baris['nip'],
				$baris['nama'],
				$baris['alamat'],
				$baris['email'],
				$baris['website']
			));
		}


	}


	public function printPDF ($date1, $date2) {

		if ($this->options['paper_size'] == "A4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation'], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation'], "pt", $this->options['paper_size']);
		}

	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("helvetica", "B", 10);
	    //$this->AddPage();

	    $this->rptDetailData($date1, $date2);

	    $this->Output($this->options['filename'],$this->options['destinationfile']);
  	}



  	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=10*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,10,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger){
			$this->AddPage($this->CurOrientation);
                        $this->headerTable();
                        $this->SetFont('Arial','',9);
                }
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}

        function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
} //end of class

//contoh penggunaan
$data = array(
	array(
		'nip'		=> 'NIHIL',
		'nama' 		=> 'NIHIL',
		'alamat' 	=> 'NIHIL',
		'email' 	=> 'NIHIL',
		'website' 	=> 'NIHIL',
	),
	
);
//pilihan
$options = array(
	'filename' => '', //nama file penyimpanan, kosongkan jika output ke browser
	'destinationfile' => '', //I=inline browser (default), F=local file, D=download
	'paper_size'=>'A4',	//paper size: F4, A3, A4, A5, Letter, Legal
	'orientation'=>'P' //orientation: P=portrait, L=landscape
);

$tabel = new laporanWipPDF($data, $options);
$tabel->printPDF($date1, $date2);

?>
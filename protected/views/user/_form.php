<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'user_name',array('class'=>'span5','maxlength'=>30)); ?>

        <?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>60)); ?>

        <?php echo $form->textFieldRow($model,'fullname',array('class'=>'span5','maxlength'=>60)); ?>

	<?php //echo $form->textFieldRow($model,'saltPassword',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->dropDownListRow($model, 'role', array('1'=>'Internal', '2'=>'BC')); ?>
        <?php //echo $form->textFieldRow($model,'role',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model, 'status', array('A'=>'Aktif', 'I'=>'Tidak Aktif')); ?>
        <?php //echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

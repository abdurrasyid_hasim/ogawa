<h1>Change Password <?php echo $model->user_name; ?></h1>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

<form action="<?php  ?>" method="post">
        <label for="new_password" class="required">New Password<span class="required">*</span></label>
            <input type="password" name="new_password" id="new_password" />

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Ganti Password',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

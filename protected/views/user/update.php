<?php
$this->breadcrumbs=array(
	'User Management /',
	$model->user_id,
	'/Update',
);

$this->menu=array(
	//array('label'=>'List User','url'=>array('index')),
	array('label'=>'Create User','url'=>array('create')),
	array('label'=>'View User','url'=>array('view','id'=>$model->user_id)),
	array('label'=>'Manage User','url'=>array('admin')),
        array('label'=>'Ganti Password','url'=>array('changePass','id'=>$model->user_id)),
);
?>

<h1>Update User <?php echo $model->user_name; ?></h1>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>
<script>

</script>
<style>
    .transaksi{
        height: 20px;
        width: 100%;
    }
</style>
<?php 
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/bootstrap/css/bootstrap.min.css');    
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap/js/bootstrap.min.js'); 
?>
<?php
$this->breadcrumbs = array(
    'Daftar Transaksi/',
    'Transaksi/',
    $tranheader->tranheader_id,
);

$this->menu = array(
//array('label'=>'List Tranheader','url'=>array('index')),
    array('label' => 'Transaksi Baru', 'url' => array('//tranheader/createNewTransaction')),
        //array('label'=>'Update Transaksi', 'url'=>array('')),
);
?>
<?php
if (!isset(Yii::app()->session["lastTransaksi"])) {
            Yii::app()->session["lastTransaksi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastTransaksi"];
        } else {
            Yii::app()->session["lastTransaksi"] = $_GET;
           // $Trandetail_page=Yii::app()->session["lastTransaksi"];
        }
        
        echo Yii::app()->session["Trandetail_page"];
?>

<h1>Transaksi <?php //echo $tranheader->tranheader_id;     ?></h1>
<?php /*
$this->widget('ext.editable.EditableField', array(
    'type'      => 'text',
    'model'     => $tranheader,
    'attribute' => 'type_tran',
    'url'       => $this->createUrl('trandetail/update'),  //url for submit data
));*/ 
?>
<?php
//function($tranheader){
if ($tranheader->type_tran == 'P') {
    $tipe = 'Pemasukan';
} else if ($tranheader->type_tran == 'K') {
    $tipe = 'Pengeluaran';
//$class=$data->status_tran;
} else if ($tranheader->type_tran == 'S') {
    $tipe = 'Penyesuaian';
} else if ($tranheader->type_tran == 'O') {
    $tipe = 'Stok Opname';
} else if ($tranheader->type_tran == 'A') {
    $tipe = 'Saldo Awal';
}

if ($tranheader->status_tran == 'P') {
    $status = 'Processed';
} else {
    $status = 'New';
}
?>
<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $tranheader,
    'attributes' => array(
        'tranheader_id',
        array(
            'name' => 'tanggal_tran',
            // 'type'=>'raw',
            'value' => Yii::app()->dateFormatter->format("d MMMM y", strtotime($tranheader->tanggal_tran)),
        ),
        array(
            'name' => 'type_tran',
            'value' => $tipe,
        ),
        array(
            'name' => 'status_tran',
            'value' => $status,
        ),
        //'user_id',
        'keterangan',
        'user.user_name',
    ),
));
?>

<div class="transaksi" >

    <?php
    //$dummy = new EKeepSelection('#transaksi-detail'); 
    
    if ($tranheader->type_tran == 'P') {
        $visible = true; //jika tipe A, kolom visible
        $hapus = CHtml::link("Hapus", "#", array("style" => 'pointer-events: none; color:gray;'));
    } else {
        $visible = false;
    }

    if ($tranheader->status_tran == 'P') {
        $processed = false;
    } else {
        $processed = true;
    }
    
    


    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'transaksi-detail',
        'dataProvider' => $trandetail->search2($tranheader->tranheader_id),
        //'filter'=>$trandetail,
        'columns' => array(
             //array('class'=>'CCheckBoxColumn'), 
            
            array(
                'name'=>'trandetail_id',
                'header' => 'No.',
                'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)
',
            ),
            array(
                'name' => 'jenis_dokumen',
                'value' => '$data->jenis_dokumen',
                'visible' => $visible,
            ),
            array(
                'name' => 'dp_nomor',
                'value' => '$data->dp_nomor',
                'visible' => $visible,
            ),
            array(
                'name' => 'dp_tanggal',
                'value' => '$data->dp_tanggal',
                'visible' => $visible,
            ),
            array(
                'name' => 'bpb_nomor',
                'value' => '$data->bpb_nomor',
                'visible' => $visible,
            ),
            array(
                'name' => 'bpb_tanggal',
                'value' => '$data->bpb_tanggal',
                'visible' => $visible,
            ),
            'kode_barang',
            'nama_barang',
            //'quantity',
            array(
                    'name'=>'quantity',
                    'value'=>'number_format($data->quantity,4,",",".")',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
            'satuan',
            //'tranheader_id',
            array(
                'name' => 'keterangan',
                'value' => '$data->keterangan',
                'visible' => $visible,
            ),
            array(
                'name' => 'Edit',
                'header' => 'Edit',
                'type' => 'raw',
                'htmlOptions' => array('width' => '40'),
                'value' => 'CHtml::link("Edit", Yii::app()->createUrl("/trandetail/update", array("idTranHeader" => $data["tranheader_id"], "id" => $data["trandetail_id"])))',
                'visible' => $processed,
            ),
            array(
                'name' => 'Hapus',
                'header' => 'Hapus',
                'type' => 'raw',
                //'value'=>'CHtml::link("Delete",Yii::app()->createUrl("/trandetail/delete"),array("submit"=>array("delete", "id"=>$data["trandetail_id"]), "confirm" => "Are you sure?")))',
                'value' => 'CHtml::link("Hapus","#", array("submit"=>array("delete", "id"=>$data["trandetail_id"]), "confirm" => "Are yous sure?"))',
                'htmlOptions' => array('width' => '40'),
                'visible' => $processed,
            ),
           
        /* array(
          'class'=>'bootstrap.widgets.TbButtonColumn',
          ), */
        ),
    ));
    //}
    ?>


<?php
if ($tranheader->status_tran == 'N') {//if N
    echo CHtml::button('+ADD', array(
        'submit' => array('trandetail/create', 'id' => $tranheader->tranheader_id),
            //'confirm' => 'Are you sure?'
            // or you can use 'params'=>array('id'=>$id)
            // 'disabled'=>'disabled',
            'class'=>'btn btn-primary',
            )
    );
    ?>

        <?php
        echo CHtml::button('PROCESS', array(
            'submit' => array('//tranheader/process', 'id' => $tranheader->tranheader_id),
                //'confirm' => 'Are you sure?'
                // or you can use 'params'=>array('id'=>$id)
            'class'=>'btn btn-success',
                )
        );
    }//endif
    ?>
    <br/>
    <br/>
    <br/>
</div>


<!--<a class="delete" title="" rel="tooltip" href="/ogawa/index.php/trandetail/delete/86" data-original-title="Delete"><i class="icon-trash"></i></a>-->

<?php
//echo CHtml::linkButton('+ADD',array('trandetail/create',
//                                 'id'=>$tranheader->tranheader_id)); 
?>

<?php
//echo CHtml::link('PROCESS',array('//tranheader/process',
//                                 'id'=>$tranheader->tranheader_id)); 
?>

<?php
//$results = Tranheader::model()->findByAttributes(array('status_tran'=>'N'));
//$count = count ( $results );
//echo $count;
?>
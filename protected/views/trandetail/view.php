<?php
$this->breadcrumbs=array(
	'Trandetails'=>array('index'),
	$model->trandetail_id,
);

$this->menu=array(
	//array('label'=>'List Trandetail','url'=>array('index')),
	//array('label'=>'Create Trandetail','url'=>array('create')),
	//array('label'=>'Update Trandetail','url'=>array('update','id'=>$model->trandetail_id)),
	//array('label'=>'Delete Trandetail','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->trandetail_id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Trandetail','url'=>array('admin')),
);
?>

<h1>View Trandetail #<?php echo $model->trandetail_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'trandetail_id',
		'kode_barang',
		'nama_barang',
		'quantity',
		'satuan',
		'tranheader_id',
	),
)); ?>

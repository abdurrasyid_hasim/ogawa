<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('trandetail_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->trandetail_id),array('view','id'=>$data->trandetail_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_barang')); ?>:</b>
	<?php echo CHtml::encode($data->kode_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_barang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuan')); ?>:</b>
	<?php echo CHtml::encode($data->satuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tranheader_id')); ?>:</b>
	<?php echo CHtml::encode($data->tranheader_id); ?>
	<br />


</div>
<?php
$this->breadcrumbs=array(
	'Trandetails'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Trandetail','url'=>array('index')),
	array('label'=>'Create Trandetail','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('trandetail-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Trandetails</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'trandetail-grid',
	'dataProvider'=>$model->search2(93),
	'filter'=>$model,
	'columns'=>array(
		'trandetail_id',
		'kode_barang',
		'nama_barang',
		'quantity',
		'satuan',
		'tranheader_id',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Trandetails',
);

$this->menu=array(
	array('label'=>'Create Trandetail','url'=>array('create')),
	array('label'=>'Manage Trandetail','url'=>array('admin')),
);
?>

<h1>Trandetails</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

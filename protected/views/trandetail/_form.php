<script type="text/javascript">

    /*function samain(idElement, idElement2, idElement3, hidden)
     {
     var element = document.getElementById(idElement);
     var val = element.selectedIndex;
     document.getElementById(idElement2).selectedIndex = val;
     document.getElementById(idElement3).selectedIndex = val;
     //document.getElementById(hidden).value=document.getElementById(idElement3).value;
     }*/

    function enable()
    {
        document.getElementById("Trandetail_satuan").disabled = false;
        document.getElementById("Trandetail_type").disabled = false;
        document.getElementById("Trandetail_id_inventory").disabled = false;

        //document.getElementById("status").disabled=false;

    }

    function fillAtributBarang()
    {
        var index = document.getElementById("Trandetail_nama_barang").selectedIndex;
        var satuan = document.getElementById("Trandetail_nama_barang").options[index].getAttribute('satuan');
        var id_inv = document.getElementById("Trandetail_nama_barang").options[index].getAttribute('id_inv');
        var type = document.getElementById("Trandetail_nama_barang").options[index].getAttribute('tipe');

        document.getElementById("Trandetail_satuan").value = satuan;
        document.getElementById("Trandetail_type").value = type;
        document.getElementById("Trandetail_id_inventory").value = id_inv;

    }

    function nullAtributBarang()
    {
        document.getElementById("Trandetail_satuan").value = "";
        document.getElementById("Trandetail_type").value = "";
        document.getElementById("Trandetail_id_inventory").value = "";

    }

</script>
<?php
///echo $idheader;
//if(isset($_GET['idheader'])){ //get id header untuk create
$header = Tranheader::model()->findByPk($idheader);
//echo $header->type_tran;
//}
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'trandetail-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('onsubmit' => 'enable()'),
        ));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php if ($header->type_tran == 'P') { ?>
    <div class="row">
        <div class="span9">
            <?php echo $form->textFieldRow($model, 'jenis_dokumen', array('class' => 'span7')); ?>
        </div>
        <div class="span4">
            <?php echo $form->textFieldRow($model, 'dp_nomor', array('class' => 'span3')); ?>
            <?php echo $form->textFieldRow($model, 'bpb_nomor', array('class' => 'span3')); ?>
        </div>

        <div class="span4">
            <label for="Trandetail_dp_tanggal">DP Tanggal</label>
            <input class="span3" type="date" id="Trandetail_dp_tanggal" name="Trandetail[dp_tanggal]" value="<?php //echo $date1;   ?>" />

            <label for="Trandetail_bpb_tanggal">BPB Tanggal</label>
            <input class="span3" type="date" id="Trandetail_bpb_tanggal" name="Trandetail[bpb_tanggal]" value="<?php //echo $date1;   ?>" />

        </div></div>
<?php } ?>

<label for="Trandetail_kode_barang" class="required">Kode Barang <span class="required">*</span></label>
<?php
echo $form->dropDownList($model, 'kode_barang', CHtml::listData(Inventorymaster::model()->findAll(
//array(
//'select'=>'*',
//'condition'=>"code NOT IN(SELECT kode_barang FROM trandetail WHERE tranheader_id ='$idheader'))")
                ), 'code', 'code'), array(
    'onChange' => 'nullAtributBarang()',
    'prompt' => '-- Pilih Kode Barang --',
    'ajax' => array(
        'type' => 'POST',
        'url' => Yii::app()->createUrl('//InventoryMaster/LoadBarang'), //or $this->createUrl('loadcities') if '$this' extends CController
        'update' => '#Trandetail_nama_barang', //or 'success' => 'function(data){...handle the data in the way you want...}',
        'data' => array('Trandetail_kode_barang' => 'js:this.value', 'YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),
)));
?>


<label for="Trandetail_nama_barang" class="required">Nama Barang <span class="required">*</span></label>
<select class="span7" onchange="fillAtributBarang()" name="Trandetail[nama_barang]" id="Trandetail_nama_barang" class="">
    <?php
    $data = new CActiveDataProvider('Inventorymaster', array(
        'criteria' => array(
            'condition' => "code='$model->kode_barang'",
        //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
        //'order'=>'vehicle_id DESC',
        )
    ));
    echo "<option value='' id_inv='' satuan='' tipe=''>-- Pilih Barang --</option>";
    foreach ($data->getData() as $i => $ii) {
        //foreach ($data2 as $value2=>$id_inventory){
        //echo CHtml::tag('option', array('value'=>$ii['description'], 'id_inv'=>$ii['id_inventory'], 'satuan'=>$ii['satuan'], 'tipe'=>$ii['type']),CHtml::encode($ii['description']),true);
        ?><option <?php if ($model->id_inventory == $ii['id_inventory']) echo "selected=selected"; ?> value="<?php echo $ii['description']; ?>" id_inv="<?php echo $ii['id_inventory']; ?>" satuan="<?php echo $ii['satuan']; ?>" tipe="<?php echo $ii['type']; ?>"><?php echo $ii['description']; ?></option>
        <?php
        //  }
    }
    ?>
</select>

<div class="row">
    <div class="span2">
<?php echo $form->textFieldRow($model, 'type', array('disabled' => 'disabled', 'class' => 'span2')); ?>
    </div>
    <div class="span3">
<?php echo $form->textFieldRow($model, 'quantity', array('class' => 'span3')); ?>
    </div>
        <div class="span2">
<?php echo $form->textFieldRow($model, 'satuan', array('disabled' => 'disabled', 'class' => 'span2')); ?>
        </div>
</div>
<?php echo $form->hiddenField($model, 'id_inventory'); ?>


<?php if ($header->type_tran == 'P') { ?>
    <?php echo $form->textAreaRow($model, 'keterangan', array('class' => 'span7')); ?>
<?php } ?>



<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'ADD' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>



<?php
$this->breadcrumbs=array(
	'Keluar Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KeluarDetail','url'=>array('index')),
	array('label'=>'Manage KeluarDetail','url'=>array('admin')),
);
?>

<h1>Create KeluarDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
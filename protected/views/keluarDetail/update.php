<?php
$this->breadcrumbs=array(
	'Keluar Details'=>array('index'),
	$model->id_keluar_detail=>array('view','id'=>$model->id_keluar_detail),
	'Update',
);

$this->menu=array(
	array('label'=>'List KeluarDetail','url'=>array('index')),
	array('label'=>'Create KeluarDetail','url'=>array('create')),
	array('label'=>'View KeluarDetail','url'=>array('view','id'=>$model->id_keluar_detail)),
	array('label'=>'Manage KeluarDetail','url'=>array('admin')),
);
?>

<h1>Update KeluarDetail <?php echo $model->id_keluar_detail; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
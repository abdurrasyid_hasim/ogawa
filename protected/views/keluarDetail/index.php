<?php
$this->breadcrumbs=array(
	'Keluar Details',
);

$this->menu=array(
	array('label'=>'Create KeluarDetail','url'=>array('create')),
	array('label'=>'Manage KeluarDetail','url'=>array('admin')),
);
?>

<h1>Keluar Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

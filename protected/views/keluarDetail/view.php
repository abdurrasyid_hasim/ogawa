<?php
$this->breadcrumbs=array(
	'Keluar Details'=>array('index'),
	$model->id_keluar_detail,
);

$this->menu=array(
	array('label'=>'List KeluarDetail','url'=>array('index')),
	array('label'=>'Create KeluarDetail','url'=>array('create')),
	array('label'=>'Update KeluarDetail','url'=>array('update','id'=>$model->id_keluar_detail)),
	array('label'=>'Delete KeluarDetail','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_keluar_detail),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KeluarDetail','url'=>array('admin')),
);
?>

<h1>View KeluarDetail #<?php echo $model->id_keluar_detail; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id_keluar_detail',
		'id_keluar_header',
		'kode_barang',
		'nama_barang',
		'jumlah',
		'satuan',
		'nilai',
	),
)); ?>

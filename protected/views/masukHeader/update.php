<?php
$this->breadcrumbs=array(
	'Masuk Headers'=>array('index'),
	$model->id_masuk_header=>array('view','id'=>$model->id_masuk_header),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasukHeader','url'=>array('index')),
	array('label'=>'Create MasukHeader','url'=>array('create')),
	array('label'=>'View MasukHeader','url'=>array('view','id'=>$model->id_masuk_header)),
	array('label'=>'Manage MasukHeader','url'=>array('admin')),
);
?>

<h1>Update MasukHeader <?php echo $model->id_masuk_header; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
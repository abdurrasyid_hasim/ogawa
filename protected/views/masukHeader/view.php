<?php
$this->breadcrumbs=array(
	'Masuk Headers'=>array('index'),
	$model->id_masuk_header,
);

$this->menu=array(
	array('label'=>'List MasukHeader','url'=>array('index')),
	array('label'=>'Create MasukHeader','url'=>array('create')),
	array('label'=>'Update MasukHeader','url'=>array('update','id'=>$model->id_masuk_header)),
	array('label'=>'Delete MasukHeader','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_masuk_header),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasukHeader','url'=>array('admin')),
);
?>

<h1>View MasukHeader #<?php echo $model->id_masuk_header; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id_masuk_header',
		'dp_jenis',
		'dp_nomor',
		'dp_tanggal',
		'bpb_nomor',
		'bpb_tanggal',
		'pengirim',
	),
)); ?>

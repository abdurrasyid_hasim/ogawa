<?php
$this->breadcrumbs=array(
	'Masuk Headers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MasukHeader','url'=>array('index')),
	array('label'=>'Manage MasukHeader','url'=>array('admin')),
);
?>

<h1>Create MasukHeader</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
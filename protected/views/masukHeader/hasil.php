<?php
$this->menu=array(
	//array('label'=>'List Tranheader','url'=>array('index')),
	array('label'=>'Transaksi Baru','url'=>array('transaction')),
        array('label'=>'Update Transaksi', 'url'=>array('')),
);
?>

<h1>View Tranheader #<?php //echo $model->tranheader_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$masukHeader,
	'attributes'=>array(
		'id_masuk_header',
		'dp_jenis',
		'dp_nomor',
		'dp_tanggal',
		'bpb_nomor',
		'bpb_tanggal',
		'pengirim',
	),
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'trandetail-grid',
	'dataProvider'=>$masukDetail,
	//'filter'=>$trandetail,
	'columns'=>array(
		//'trandetail_id',
		'id_masuk_detail',
		'id_masuk_header',
		'kode_barang',
		'nama_barang',
		'jumlah',
		'satuan',
		'nilai',
		//array(
		//	'class'=>'bootstrap.widgets.TbButtonColumn',
		//),
	),
)); ?>


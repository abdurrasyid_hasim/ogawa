<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_masuk_header')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_masuk_header),array('view','id'=>$data->id_masuk_header)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_jenis')); ?>:</b>
	<?php echo CHtml::encode($data->dp_jenis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_nomor')); ?>:</b>
	<?php echo CHtml::encode($data->dp_nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dp_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->dp_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpb_nomor')); ?>:</b>
	<?php echo CHtml::encode($data->bpb_nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bpb_tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->bpb_tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengirim')); ?>:</b>
	<?php echo CHtml::encode($data->pengirim); ?>
	<br />


</div>
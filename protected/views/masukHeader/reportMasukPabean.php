<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>
<style>
  //  .search-table-outter {border:2px solid #005580;}
//.search-table{table-layout: auto; margin:0px auto 0px auto; }
//.search-table, td, th{border-collapse:collapse; border:1px solid #777;}
//th{padding:20px 7px; font-size:15px; color:#444; background:#f0f0f0;}
//td{padding:5px 10px; height:35px;}

//.search-table-outter { overflow-x: scroll;  overflow-y: hidden;  }
//th, td { min-width: 130px; }
.table td{
    font-size: 8pt;
}
</style>
<script>
 function submitFunction(i) {
    if (i==1) {
        document.theForm.target="";
        document.theForm.action=
      "<?php echo Yii::app()->createUrl('masukHeader/reportMasukPabean'); ?>";
      }
   if (i==2) {
       document.theForm.target="_blank";
       document.theForm.action=
      "<?php echo Yii::app()->createUrl('masukHeader/masukPabeanPDF'); ?>";
      }
   document.theForm.submit()
   }
</script>

<h1>Laporan Pemasukan Barang Per Dokumen Pabean</h1>
<br>

    <form name="theForm" method="post" target="">
            Dari Tanggal: 
            <?php 
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date1',
     'value'=>date('d-m-Y', strtotime($date1)),
     'id'=>'date1',
    // additional javascript options for the date picker plugin
    'options'=>array(
        
//'showAnim' => 'fold',
'dateFormat' => 'dd-mm-yy', // save to db format
//'altField' => 'date1',
'altFormat' => 'yy-mm-dd', // show to user format
),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
  //      'id'=>'date1',
    ),
));
 ?>
            </td>
            <td>&nbsp;&nbsp;</td><td>Sampai :</td><td>
                
     <?php 
     
 $this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'date2',
     'id'=>'date2',
     'value'=>date('d-m-Y', strtotime($date2)),
     
    // additional javascript options for the date picker plugin
    'options'=>array(
       // 'showAnim'=>'fold',
        'dateFormat'=>'dd-mm-yy',
       'altFormat'=>'yy-mm-dd',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;',
    //    'id'=>'date2',
    ),
));
 ?>
            </td></tr>
        <input class="btn btn-primary" name="submit2" type="submit" value="Load" onclick="submitFunction(1)">
        <input class="btn btn-primary" name="submit" type="submit" value="Eksport" onclick="submitFunction(2)"/>
    </form>

<?php
//echo Yii::app()->dateFormatter->format("d MMM y",strtotime($date1))."<br>";
//echo Yii::app()->dateFormatter->format("d MMM y",strtotime($date2));
/*
echo CHtml::button('RESET',
    array(
        'submit'=>array('masukHeader/resetMasukReport'),
        //'confirm' => 'Are you sure?'
        // or you can use 'params'=>array('id'=>$id)
       // 'disabled'=>'disabled',
    )
);*/
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'RESET',
    'type'=>'warning', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'mini', // null, 'large', 'small' or 'mini'
    'url'=>array('masukHeader/resetMasukReport'),
    
)); ?>


<?php  $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'masuk-header-grid',
	'dataProvider'=>$model,
	//'filter'=>$model,
        'type'=>'striped',
        //'template'=>"{items}\n{pager}",
        'htmlOptions'=>array('style'=>'text-size:8pt;'),
	'columns'=>array(
		//'id_masuk_header',
                 /*array(
                     'name'=>'id_masuk_header',
                     'header'=>'No',
                     'htmlOptions'=>array('style'=>'text-align:right;'),
                 ),*/
            array('header'=>'No.',
          'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            ),
                'dp_jenis',
                 //'idMasukHeader.dp_jenis',
                 'dp_nomor',
                 //'idMasukHeader.dp_nomor',
                 array(
                     'name'=>'dp_tanggal',
                     'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->dp_tanggal))',
                     ),
            /*array(
                'name'=>'idMasukHeader.dp_tanggal',
                'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->idMasukHeader->dp_tanggal))',
                ),*/
            'bpb_nomor',
            //'idMasukHeader.bpb_nomor',
            array(
                     'name'=>'bpb_tanggal',
                     'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->bpb_tanggal))',
                     ),
            /*array(
                'name'=>'idMasukHeader.bpb_tanggal',
                'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->idMasukHeader->bpb_tanggal))',
                ),*/
            'pengirim',
            //'idMasukHeader.pengirim',
		'kode_barang',
		'nama_barang',
		array(
                    'name'=>'jumlah',
                    'value'=>'number_format($data->jumlah,4,",",".")',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
		'satuan',
                array(
                    'name'=>'nilai',
                    'value'=>'number_format($data->nilai,2,",",".")',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    ),
                
            
	
)));  ?>



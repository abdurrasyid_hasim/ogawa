<?php
$this->breadcrumbs=array(
	'Masuk Headers',
);

$this->menu=array(
	array('label'=>'Create MasukHeader','url'=>array('create')),
	array('label'=>'Manage MasukHeader','url'=>array('admin')),
);
?>

<h1>Masuk Headers</h1>
<table border="1" class="search-table inner table table-striped" >
    <thead style="font-size: 11pt;">
        <tr>
            <th rowspan="2" style="width:10;"> No.</th>
             <th rowspan="2">Jenis Dokumen</th>
            <th colspan="2">Dokumen Pabean</th>
        
            <th colspan="2">Bukti Penerimaan Barang</th>
        
             <th rowspan="2">Pemasok/Pengirim</th>
            <th rowspan="2">Kode Barang</th>
        <th rowspan="2">Nama Barang</th>
        <th rowspan="2">Jumlah</th>
        <th rowspan="2">Satuan</th>
        <th rowspan="2">Nilai (IDR)</th>   
        </tr>
        
        <tr>
        
        <th>Nomor</th>
        <th>Tanggal</th>
        <th>Nomor</th>
        <th>Tanggal</th>
        
        </tr>
    </thead>
</table>
<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
$this->breadcrumbs=array(
	'Masuk Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MasukDetail','url'=>array('index')),
	array('label'=>'Manage MasukDetail','url'=>array('admin')),
);
?>

<h1>Create MasukDetail</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
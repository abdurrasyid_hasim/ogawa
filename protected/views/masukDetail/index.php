<?php
$this->breadcrumbs=array(
	'Masuk Details',
);

$this->menu=array(
	array('label'=>'Create MasukDetail','url'=>array('create')),
	array('label'=>'Manage MasukDetail','url'=>array('admin')),
);
?>

<h1>Masuk Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
$this->breadcrumbs=array(
	'Masuk Details'=>array('index'),
	$model->id_masuk_detail,
);

$this->menu=array(
	array('label'=>'List MasukDetail','url'=>array('index')),
	array('label'=>'Create MasukDetail','url'=>array('create')),
	array('label'=>'Update MasukDetail','url'=>array('update','id'=>$model->id_masuk_detail)),
	array('label'=>'Delete MasukDetail','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id_masuk_detail),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MasukDetail','url'=>array('admin')),
);
?>

<h1>View MasukDetail #<?php echo $model->id_masuk_detail; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id_masuk_detail',
		'id_masuk_header',
		'kode_barang',
		'nama_barang',
		'jumlah',
		'satuan',
		'nilai',
	),
)); ?>

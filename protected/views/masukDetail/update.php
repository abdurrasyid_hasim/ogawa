<?php
$this->breadcrumbs=array(
	'Masuk Details'=>array('index'),
	$model->id_masuk_detail=>array('view','id'=>$model->id_masuk_detail),
	'Update',
);

$this->menu=array(
	array('label'=>'List MasukDetail','url'=>array('index')),
	array('label'=>'Create MasukDetail','url'=>array('create')),
	array('label'=>'View MasukDetail','url'=>array('view','id'=>$model->id_masuk_detail)),
	array('label'=>'Manage MasukDetail','url'=>array('admin')),
);
?>

<h1>Update MasukDetail <?php echo $model->id_masuk_detail; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
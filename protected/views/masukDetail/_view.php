<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_masuk_detail')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_masuk_detail),array('view','id'=>$data->id_masuk_detail)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_masuk_header')); ?>:</b>
	<?php echo CHtml::encode($data->id_masuk_header); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_barang')); ?>:</b>
	<?php echo CHtml::encode($data->kode_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_barang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_barang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuan')); ?>:</b>
	<?php echo CHtml::encode($data->satuan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai')); ?>:</b>
	<?php echo CHtml::encode($data->nilai); ?>
	<br />


</div>
<?php
require('fpdf.php');

class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    $this->Image('bpn.jpg',1,2,2.5,2.5);
    // Arial bold 15
    $this->SetFont('Times','B',15);
    // Move to the right
    //$this->Cell(8);
    // Title
    $this->Cell(0,2.54,'BADAN PERTANAHAN NASIONAL REPUBLIK INDONESIA',0,0,'C');
	$this->ln(0.75);
	$this->Cell(0,2.54,'KANTOR PERTANAHAN KOTA PEKANBARU',0,0,'C');
	$this->ln(0.75);
	$this->Cell(0,2.54,'PROVINSI RIAU',0,0,'C');
	$this->ln(0.75);
	$this->SetFont('Times','',13);
	$this->Cell(0,2.54,'JALAN PEPAYA NO.47 TELP.(0761)23106-PEKANBARU',0,0,'C');
	$this->ln(0.75);
	$this->SetLineWidth(0.07);
	$this->line(0.54,5,21,5);	
}

function ChapterBody($file)
{
    // Read text file

    $txt = file_get_contents($file);
	
    // Font
    $this->SetFont('Times','',12);
    // Output text in a 6 cm width column
	// $this->Cell(0,0);
	 $this->Ln(10);
    $this->MultiCell(21,0.5,$txt);
	
    $this->Ln();


}

}
// Instanciation of inherited class
$pdf = new PDF('P','cm',array(21.59,33));
$pdf->AddPage();
$pdf->ChapterBody('coba.txt');
//$pdf->SetFont('Times','',12);
$pdf->Output();
?>
<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $fullname
 * @property string $password
 * @property string $saltPassword
 * @property integer $role
 * @property integer $status
 * @property string $user_name
 */
class User extends CActiveRecord
{
    public $old_password;
    public $new_password;
    public $repeat_password;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
   
                        /*array('old_password, new_password, repeat_password', 'required', 'on' => 'changePwd'),
                        array('old_password', 'findPasswords', 'on' => 'changePwd'),
                        array('repeat_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'changePwd'),
    */
			array('fullname, password, saltPassword, role, status, user_name', 'required'),
                        array('user_name','unique','message'=>'{attribute}:{value} already exist!'),
			array('role', 'numerical', 'integerOnly'=>true),
			array('fullname, password', 'length', 'max'=>60),
			array('saltPassword, user_name', 'length', 'max'=>30),
                        array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, fullname, password, saltPassword, role, status, user_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'inventorymasters' => array(self::HAS_MANY, 'Inventorymaster', 'user_id'),
                    'logs' => array(self::HAS_MANY, 'Log', 'user_id'),
                    //'role' => array(self::BELONGS_TO, 'Role', 'role'),
                    'role0' => array(self::BELONGS_TO, 'Role', 'role'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User ID',
			'fullname' => 'Fullname',
			'password' => 'Password',
			'saltPassword' => 'Salt Password',
			'role' => 'Role',
			'status' => 'Status',
			'user_name' => 'User Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with=array('inventorymasters', 'logs', 'role0');
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('saltPassword',$this->saltPassword,true);
		$criteria->compare('role0.role_name',$this->role,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('user_name',$this->user_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function validatePassword($password)
        {
            return $this->hashPassword($password,$this->saltPassword)===$this->password;
            
        }
        
        public function hashPassword($password,$salt)
        {
            return md5($salt.$password);
            
        }
        
        public function generateSalt()
        {
            return uniqid('',true);
        }
        
        /*public function findPasswords($attribute, $params)
        {
        $user = User::model()->findByPk(Yii::app()->user->user_id);
        if ($this->hashPassword($password,$this->saltPassword)!=$this->password)
            $this->addError($attribute, 'Old password is incorrect.');
        }*/
        
       

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	
}
<?php

/**
 * This is the model class for table "report_pemasukan_bea".
 *
 * The followings are the available columns in table 'report_pemasukan_bea':
 * @property integer $lap_masuk_id
 * @property string $dp_jenis
 * @property integer $dp_no
 * @property string $dp_tanggal
 * @property integer $bpb_no
 * @property string $bpb_tanggal
 * @property string $pengirim_barang
 * @property string $kode_barang
 * @property string $nama_barang
 * @property integer $jumlah
 * @property string $satuan
 * @property double $nilai
 * @property integer $id_user
 */
class ReportPemasukanBea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReportPemasukanBea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'report_pemasukan_bea';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lap_masuk_id, dp_jenis, dp_no, dp_tanggal, bpb_no, bpb_tanggal, pengirim_barang, kode_barang, nama_barang, jumlah, satuan, nilai, id_user', 'required'),
			array('lap_masuk_id, dp_no, bpb_no, jumlah, id_user', 'numerical', 'integerOnly'=>true),
			array('nilai', 'numerical'),
			array('dp_jenis', 'length', 'max'=>10),
			array('satuan', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('lap_masuk_id, dp_jenis, dp_no, dp_tanggal, bpb_no, bpb_tanggal, pengirim_barang, kode_barang, nama_barang, jumlah, satuan, nilai, id_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lap_masuk_id' => 'No Laporan Masuk',
			'dp_jenis' => 'Dp Jenis',
			'dp_no' => 'Dp No',
			'dp_tanggal' => 'Dp Tanggal',
			'bpb_no' => 'Bpb No',
			'bpb_tanggal' => 'Bpb Tanggal',
			'pengirim_barang' => 'Pengirim Barang',
			'kode_barang' => 'Kode Barang',
			'nama_barang' => 'Nama Barang',
			'jumlah' => 'Jumlah',
			'satuan' => 'Satuan',
			'nilai' => 'Nilai',
			'id_user' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lap_masuk_id',$this->lap_masuk_id);
		$criteria->compare('dp_jenis',$this->dp_jenis,true);
		$criteria->compare('dp_no',$this->dp_no);
		$criteria->compare('dp_tanggal',$this->dp_tanggal,true);
		$criteria->compare('bpb_no',$this->bpb_no);
		$criteria->compare('bpb_tanggal',$this->bpb_tanggal,true);
		$criteria->compare('pengirim_barang',$this->pengirim_barang,true);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('nilai',$this->nilai);
		$criteria->compare('id_user',$this->id_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
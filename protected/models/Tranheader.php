<?php

/**
 * This is the model class for table "tranheader".
 *
 * The followings are the available columns in table 'tranheader':
 * @property integer $tranheader_id
 * @property string $tanggal_tran
 * @property string $type_tran
 * @property string $status_tran
 * @property integer $user_id
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property Trandetail[] $trandetails
 * @property User $user
 */
class Tranheader extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tranheader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tranheader';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tanggal_tran, type_tran, status_tran, user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('type_tran, status_tran', 'length', 'max'=>1),
                        array('keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tranheader_id, tanggal_tran, type_tran, status_tran, user_id,keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trandetails' => array(self::HAS_MANY, 'Trandetail', 'tranheader_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tranheader_id' => 'Nomor Transaksi',
			'tanggal_tran' => 'Tanggal',
			'type_tran' => 'Tipe Transaksi',
			'status_tran' => 'Status',
			'user_id' => 'User',
                        'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                //$criteria->order = "tranheader_id DESC";
                $criteria->with=array('user');
		$criteria->compare('tranheader_id',$this->tranheader_id);
		$criteria->compare('tanggal_tran',$this->tanggal_tran,true);
		$criteria->compare('type_tran',$this->type_tran,true);
		$criteria->compare('status_tran',$this->status_tran,true);
		$criteria->compare('user.user_name',$this->user_id,true);
                $criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort'=>array(
                            'defaultOrder'=>'tranheader_id DESC',
                        ),
		));
	}
}
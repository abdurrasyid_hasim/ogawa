<?php

/**
 * This is the model class for table "trandetail".
 *
 * The followings are the available columns in table 'trandetail':
 * @property integer $trandetail_id
 * @property integer $id_inventory
 * @property string $kode_barang
 * @property string $nama_barang
 * @property string $type
 * @property double $quantity
 * @property string $satuan
 * @property integer $tranheader_id
 * @property string $jenis_dokumen
 * @property string $dp_nomor
 * @property string $dp_tanggal
 * @property string $bpb_nomor
 * @property string $bpb_tanggal
 * @property string $keterangan
 *
 * The followings are the available model relations:
 * @property Tranheader $tranheader
 */
class Trandetail extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Trandetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trandetail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_inventory, kode_barang, nama_barang, type, quantity, satuan, tranheader_id', 'required'),
			array('id_inventory, tranheader_id', 'numerical', 'integerOnly'=>true),
                        array('quantity', 'numerical'),
			array('kode_barang, type, jenis_dokumen, dp_nomor, bpb_nomor', 'length', 'max'=>30),
			array('satuan', 'length', 'max'=>20),
			array('dp_tanggal, bpb_tanggal, keterangan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('trandetail_id, id_inventory, kode_barang, nama_barang, type, quantity, satuan, tranheader_id, jenis_dokumen, dp_nomor, dp_tanggal, bpb_nomor, bpb_tanggal, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tranheader' => array(self::BELONGS_TO, 'Tranheader', 'tranheader_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trandetail_id' => 'Trandetail',
			'id_inventory' => 'Id Inventory',
			'kode_barang' => 'Kode Barang',
			'nama_barang' => 'Nama Barang',
			'type' => 'Type',
			'quantity' => 'Quantity',
			'satuan' => 'Satuan',
			'tranheader_id' => 'Tranheader',
			'jenis_dokumen' => 'Jenis Dokumen',
			'dp_nomor' => 'DP Nomor',
			'dp_tanggal' => 'DP Tanggal',
			'bpb_nomor' => 'BPB Nomor',
			'bpb_tanggal' => 'BPB Tanggal',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trandetail_id',$this->trandetail_id);
		$criteria->compare('id_inventory',$this->id_inventory);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('tranheader_id',$this->tranheader_id);
		$criteria->compare('jenis_dokumen',$this->jenis_dokumen,true);
		$criteria->compare('dp_nomor',$this->dp_nomor,true);
		$criteria->compare('dp_tanggal',$this->dp_tanggal,true);
		$criteria->compare('bpb_nomor',$this->bpb_nomor,true);
		$criteria->compare('bpb_tanggal',$this->bpb_tanggal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function search2($id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->condition='tranheader_id='.$id;

		$criteria->compare('trandetail_id',$this->trandetail_id);
		$criteria->compare('id_inventory',$this->id_inventory);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('tranheader_id',$this->tranheader_id);
		$criteria->compare('jenis_dokumen',$this->jenis_dokumen,true);
		$criteria->compare('dp_nomor',$this->dp_nomor,true);
		$criteria->compare('dp_tanggal',$this->dp_tanggal,true);
		$criteria->compare('bpb_nomor',$this->bpb_nomor,true);
		$criteria->compare('bpb_tanggal',$this->bpb_tanggal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(), // optional line 
                'defaultStickOnClear' => false   //optional line 
            ),
        );
    }

}
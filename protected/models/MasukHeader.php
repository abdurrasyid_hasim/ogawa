<?php

/**
 * This is the model class for table "masuk_header".
 *
 * The followings are the available columns in table 'masuk_header':
 * @property integer $id_masuk_header
 * @property string $dp_jenis
 * @property string $dp_nomor
 * @property string $dp_tanggal
 * @property string $bpb_nomor
 * @property string $bpb_tanggal
 * @property string $pengirim
 *
 * The followings are the available model relations:
 * @property MasukDetail[] $masukDetails
 */
class MasukHeader extends CActiveRecord
{
    public $filee;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'masuk_header';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dp_jenis, dp_nomor, dp_tanggal, bpb_nomor, bpb_tanggal, pengirim', 'required'),
			array('dp_jenis', 'length', 'max'=>10),
			array('dp_nomor, bpb_nomor', 'length', 'max'=>30),
			array('pengirim', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_masuk_header, dp_jenis, dp_nomor, dp_tanggal, bpb_nomor, bpb_tanggal, pengirim', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'masukDetails' => array(self::HAS_MANY, 'MasukDetail', 'id_masuk_header'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_masuk_header' => 'Id Masuk Header',
			'dp_jenis' => 'Jenis Dokumen',
			'dp_nomor' => 'DP Nomor',
			'dp_tanggal' => 'DP Tanggal',
			'bpb_nomor' => 'BPB Nomor',
			'bpb_tanggal' => 'BPB Tanggal',
			'pengirim' => 'Pengirim/ Penerima',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_masuk_header',$this->id_masuk_header);
		$criteria->compare('dp_jenis',$this->dp_jenis,true);
		$criteria->compare('dp_nomor',$this->dp_nomor,true);
		$criteria->compare('dp_tanggal',$this->dp_tanggal,true);
		$criteria->compare('bpb_nomor',$this->bpb_nomor,true);
		$criteria->compare('bpb_tanggal',$this->bpb_tanggal,true);
		$criteria->compare('pengirim',$this->pengirim,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasukHeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getJoinDetail($date1,$date2)
	{
                
		$sql="SELECT * FROM `masuk_header`, masuk_detail where masuk_detail.id_masuk_header=masuk_header.id_masuk_header"
                        . " AND masuk_header.bpb_tanggal BETWEEN '$date1' AND '$date2'";

                $dataProvider3= new CSqlDataProvider($sql,array(
			'keyField' => 'id_masuk_header',
			'pagination'=>array(
				'pageSize'=>20000,
			),
		));
		
		return $dataProvider3;
	}
}

<?php

/**
 * This is the model class for table "keluar_detail".
 *
 * The followings are the available columns in table 'keluar_detail':
 * @property integer $id_keluar_detail
 * @property integer $id_keluar_header
 * @property string $kode_barang
 * @property string $nama_barang
 * @property double $jumlah
 * @property string $satuan
 * @property double $nilai
 *
 * The followings are the available model relations:
 * @property KeluarHeader $idKeluarHeader
 */
class KeluarDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keluar_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_keluar_header, kode_barang, nama_barang, jumlah, satuan, nilai', 'required'),
			array('id_keluar_header', 'numerical', 'integerOnly'=>true),
			array('jumlah, nilai', 'numerical'),
			array('kode_barang', 'length', 'max'=>30),
			array('nama_barang', 'length', 'max'=>60),
			array('satuan', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_keluar_detail, id_keluar_header, kode_barang, nama_barang, jumlah, satuan, nilai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKeluarHeader' => array(self::BELONGS_TO, 'KeluarHeader', 'id_keluar_header'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_keluar_detail' => 'Id Keluar Detail',
			'id_keluar_header' => 'Id Keluar Header',
			'kode_barang' => 'Kode Barang',
			'nama_barang' => 'Nama Barang',
			'jumlah' => 'Jumlah',
			'satuan' => 'Satuan',
			'nilai' => 'Nilai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_keluar_detail',$this->id_keluar_detail);
		$criteria->compare('id_keluar_header',$this->id_keluar_header);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('nilai',$this->nilai);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KeluarDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

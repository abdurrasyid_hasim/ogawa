<?php

/**
 * This is the model class for table "vmasuk".
 *
 * The followings are the available columns in table 'vmasuk':
 * @property integer $id_masuk_header
 * @property string $bpb_nomor
 * @property string $bpb_tanggal
 * @property string $dp_jenis
 * @property string $dp_nomor
 * @property string $dp_tanggal
 * @property string $pengirim
 * @property integer $id_masuk_detail
 * @property double $jumlah
 * @property string $kode_barang
 * @property string $nama_barang
 * @property double $nilai
 * @property string $satuan
 */
class Vmasuk extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Vmasuk the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vmasuk';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bpb_nomor, bpb_tanggal, dp_jenis, dp_nomor, dp_tanggal, pengirim, jumlah, kode_barang, nama_barang, nilai, satuan', 'required'),
			array('id_masuk_header, id_masuk_detail', 'numerical', 'integerOnly'=>true),
			array('jumlah, nilai', 'numerical'),
			array('bpb_nomor, dp_nomor, kode_barang', 'length', 'max'=>30),
			array('dp_jenis, satuan', 'length', 'max'=>10),
			array('pengirim', 'length', 'max'=>100),
			array('nama_barang', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_masuk_header, bpb_nomor, bpb_tanggal, dp_jenis, dp_nomor, dp_tanggal, pengirim, id_masuk_detail, jumlah, kode_barang, nama_barang, nilai, satuan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_masuk_header' => 'Id Masuk Header',
			'bpb_nomor' => 'BPB Nomor',
			'bpb_tanggal' => 'BPB Tanggal',
			'dp_jenis' => 'DP Jenis',
			'dp_nomor' => 'DP Nomor',
			'dp_tanggal' => 'DP Tanggal',
			'pengirim' => 'Pemasok/Pengirim',
			'id_masuk_detail' => 'Id Masuk Detail',
			'jumlah' => 'Jumlah',
			'kode_barang' => 'Kode Barang',
			'nama_barang' => 'Nama Barang',
			'nilai' => 'Nilai',
			'satuan' => 'Satuan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_masuk_header',$this->id_masuk_header);
		$criteria->compare('bpb_nomor',$this->bpb_nomor,true);
		$criteria->compare('bpb_tanggal',$this->bpb_tanggal,true);
		$criteria->compare('dp_jenis',$this->dp_jenis,true);
		$criteria->compare('dp_nomor',$this->dp_nomor,true);
		$criteria->compare('dp_tanggal',$this->dp_tanggal,true);
		$criteria->compare('pengirim',$this->pengirim,true);
		$criteria->compare('id_masuk_detail',$this->id_masuk_detail);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('nilai',$this->nilai);
		$criteria->compare('satuan',$this->satuan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
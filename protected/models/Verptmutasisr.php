<?php

/**
 * This is the model class for table "ve_rptmutasisr".
 *
 * The followings are the available columns in table 've_rptmutasisr':
 * @property integer $report_id
 * @property string $code
 * @property string $description
 * @property string $satuan
 * @property integer $SaldoAwal
 * @property integer $Pemasukan
 * @property integer $Pengeluaran
 * @property integer $Penyesuain
 * @property integer $SaldoBuku
 * @property integer $StockOpname
 * @property integer $Selisih
 */
class Verptmutasisr extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Verptmutasisr the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 've_rptmutasisr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, description, satuan', 'required'),
			array('report_id, SaldoAwal, Pemasukan, Pengeluaran, Penyesuain, SaldoBuku, StockOpname, Selisih', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>30),
			array('satuan', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('report_id, code, description, satuan, SaldoAwal, Pemasukan, Pengeluaran, Penyesuain, SaldoBuku, StockOpname, Selisih', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'report_id' => 'Report',
			'code' => 'Kode Barang',
			'description' => 'Deskripsi',
			'satuan' => 'Satuan',
			'SaldoAwal' => 'Saldo Awal',
			'Pemasukan' => 'Pemasukan',
			'Pengeluaran' => 'Pengeluaran',
			'Penyesuain' => 'Penyesuaian',
			'SaldoBuku' => 'Saldo Buku',
			'StockOpname' => 'Stock Opname',
			'Selisih' => 'Selisih',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('report_id',$this->report_id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('SaldoAwal',$this->SaldoAwal);
		$criteria->compare('Pemasukan',$this->Pemasukan);
		$criteria->compare('Pengeluaran',$this->Pengeluaran);
		$criteria->compare('Penyesuain',$this->Penyesuain);
		$criteria->compare('SaldoBuku',$this->SaldoBuku);
		$criteria->compare('StockOpname',$this->StockOpname);
		$criteria->compare('Selisih',$this->Selisih);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
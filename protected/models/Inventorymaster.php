<?php

/**
 * This is the model class for table "inventorymaster".
 *
 * The followings are the available columns in table 'inventorymaster':
 * @property integer $id_inventory
 * @property string $code
 * @property string $inventory
 * @property string $description
 * @property string $type
 * @property string $satuan
 * @property string $status
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Inventorymaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventorymaster';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, description, type, satuan, status, user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>30),
                        //array('code','unique','message'=>'{attribute}:{value} already exist!'),
			array('type, satuan', 'length', 'max'=>20),
			array('status', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_inventory, code, description, type, satuan, status, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_inventory' => 'Id Inventory',
			'code' => 'Kode Barang',
			//'inventory' => 'Nama Barang',
			'description' => 'Deskripsi',
                        'type' => 'Tipe',
			'satuan' => 'Satuan',
			'status' => 'Status',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_inventory',$this->id_inventory);
		$criteria->compare('code',$this->code,true);
		//$criteria->compare('inventory',$this->inventory,true);
		$criteria->compare('description',$this->description,true);
                $criteria->compare('type',$this->type,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inventorymaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getNotInTranheader($id)
	{
                
		$sql="SELECT * FROM `inventorymaster` WHERE code NOT IN ('SELECT * FROM `trandetail` WHERE tranheader_id=`$id`')";

                $dataProvider= new CSqlDataProvider($sql,array(
			'keyField' => 'id_inventory',
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
		
		return $dataProvider;
	}
        
        public function getReportWIP()
        {
            $sql='SELECT * 
                FROM trandetail, inventorymaster
                WHERE trandetail.kode_barang = inventorymaster.code
                AND inventorymaster.type="d"';

                $dataProvider= new CSqlDataProvider($sql,array(
			'keyField' => 'id_inventory',
			'pagination'=>array(
				'pageSize'=>100,
			),
		));
		
		return $dataProvider;
        }
}

<?php

class KeluarHeaderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ReportKeluarPabean','KeluarPabeanPDF','ResetKeluarReport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','importxml','create','update'),
				//'users'=>array('admin'),
                            'expression' => '$user->getRole()<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new KeluarHeader;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KeluarHeader']))
		{
			$model->attributes=$_POST['KeluarHeader'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_keluar_header));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KeluarHeader']))
		{
			$model->attributes=$_POST['KeluarHeader'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_keluar_header));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KeluarHeader');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KeluarHeader('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KeluarHeader']))
			$model->attributes=$_GET['KeluarHeader'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KeluarHeader::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='keluar-header-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



        public function actionImportxml() {
        $model = new KeluarHeader;
        $msg = '0';

        if (isset($_POST['KeluarHeader'])) {
            $model->attributes = $_POST['KeluarHeader'];
            $itu = CUploadedFile::getInstance($model, 'file');
            if ($itu != NULL) {

                $path = Yii::app()->basePath . '/../jajal.xml';
                $itu->saveAs($path);
                
                $fileinfo = pathinfo($itu);
                if ($fileinfo['extension'] != 'xml') {
                    throw new CHttpException(' :File extension must be XML');
                }


                $xml = simplexml_load_file($path);

               foreach($xml->children() as $child) {

                   $keluarHeader= new KeluarHeader;
                   $keluarHeader->bpb_nomor= $child->BPBNomor;
                   
                   //format tanggal
                    $dateform= new DateTime($child->BPBTanggal); 
                    $keluarHeader->bpb_tanggal = $dateform->format('Y-m-d H:i:s');
                  
                   $keluarHeader->dp_jenis= $child->DPJenis;
                   $keluarHeader->dp_nomor= $child->DPNomor;
                   
                   //format tanggal
                    $dateform= new DateTime($child->DPTanggal);
                    $keluarHeader->dp_tanggal = $dateform->format('Y-m-d H:i:s');
                   $keluarHeader->pengirim= $child->Pengirim;

                   if($keluarHeader->validate()){
                       $keluarHeader->save();
                      // $this->redirect('hasil');
                   }else{
                       echo "terjadi kesalahan pemasukan data pada HEADER. Mohon dicek kelengkapan data dan tipe datanya";

                   }

                   $keluarHeaderId=$keluarHeader->id_keluar_header;
                foreach($child->children() as $grandchild){

                        if($grandchild->getName()=='Detail'){
                            //echo $grandchild->getName() . ": " . $grandchild->KodeBarang . " | " . $grandchild->NamaBarang ." | " . $grandchild->Jumlah . " | " . $grandchild->Satuan . " | " . $grandchild->Nilai ."<br>";
                            $keluarDetail=new KeluarDetail();
                            $keluarDetail->id_keluar_header =$keluarHeaderId;
                            $keluarDetail->jumlah=floatval($grandchild->Jumlah);
                            $keluarDetail->kode_barang= $grandchild->KodeBarang;
                            $keluarDetail->nama_barang=$grandchild->NamaBarang;
                            $keluarDetail->nilai=floatval($grandchild->Nilai);
                            $keluarDetail->satuan=$grandchild->Satuan;

                               if($keluarDetail->validate()){
                                    $keluarDetail->save();
                                    $msg = '1';
                                }else{
                                    echo "terjadi kesalahan pemasukan data pada DETAIL. Mohon dicek kelengkapan data dan tipe datanya";
                                }

                        }
                  }
            }


                unlink($path);
                //$this->redirect('admin');
                $this->redirect(array('importxml', 'msg' => $msg));

            }
        }

        $this->render('importxml', array('model' => $model, 'msg' => $msg));
    }
    
    public function actionReportKeluarPabean()
        {
        $this->layout='//layouts/main';
            //$model = new MasukHeader('search');
        //$model->unsetAttributes();  // clear any default values
        if(isset(Yii::app()->request->cookies['ckeluardate1']->value)){
        $date1=Yii::app()->request->cookies['ckeluardate1']->value;
        $date2=Yii::app()->request->cookies['ckeluardate2']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
        }
        
        /*$report=new CActiveDataProvider('keluardetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idKeluarHeader'=>array('joinType'=>'LEFT JOIN')),
                )));*/
        $report=new CActiveDataProvider('Vkeluar', array(
            'sort'=>array(
    'defaultOrder'=>'bpb_tanggal ASC',
  ),
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                //'order'=>'bpb_tanggal ASC',
            ),
        ));
        
        if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
            
            Yii::app()->request->cookies['ckeluardate1'] = new CHttpCookie('ckeluardate1', $date1);
            Yii::app()->request->cookies['ckeluardate2'] = new CHttpCookie('ckeluardate2', $date2);
        /*$report=new CActiveDataProvider('keluardetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idKeluarHeader'=>array('joinType'=>'LEFT JOIN')),
                'order'=>'bpb_tanggal ASC',
            )
        ));*/
        $report=new CActiveDataProvider('Vkeluar', array(
            'sort'=>array(
    'defaultOrder'=>'bpb_tanggal ASC',
  ),
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                //'order'=>'bpb_tanggal ASC',
            ),
        ));
        $this->render('reportKeluarPabean', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportKeluarPabean', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
        
    }
    
    public function actionKeluarPabeanPDF(){
        //$this->layout='//layouts/main';
            //$model = new MasukHeader('search');
        //$model->unsetAttributes();  // clear any default values
        //if(isset(Yii::app()->request->cookies['ckeluardate1']->value)){
        //$date1=Yii::app()->request->cookies['ckeluardate1']->value;
        //$date2=Yii::app()->request->cookies['ckeluardate2']->value;
        //}else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
        //}

        $report=new CActiveDataProvider('keluardetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idKeluarHeader'=>array('joinType'=>'LEFT JOIN')),
                )));
        //$report=new CActiveDataProvider('masukdetail');
        if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));

                $criteria= new CDbCriteria;
                $criteria->addBetweenCondition('bpb_tanggal', $date1, $date2);

        $report=new CActiveDataProvider('keluardetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idKeluarHeader'=>array('joinType'=>'LEFT JOIN')),
                'order'=>'bpb_tanggal ASC',
                
            ),
            'pagination'=>array(
        'pageSize'=>20000,
    ),
        ));
        $this->renderPartial('keluarPabeanPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{

        $this->renderPartial('keluarPabeanPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
    }
    
    public function actionResetKeluarReport()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['ckeluardate1']);
            unset(Yii::app()->request->cookies['ckeluardate2']);
            $this->redirect('reportKeluarPabean');
        }

}

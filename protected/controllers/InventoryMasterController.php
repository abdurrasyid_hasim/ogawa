<?php

class InventorymasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ReportWIP','LaporanWipPDF','resetWIPReport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','admin','delete','index','create','update','LoadBarang','LoadBarang2'),
				//'users'=>array('admin'),
                                'expression'=>'$user->getRole()<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Inventorymaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Inventorymaster']))
		{
			$model->attributes=$_POST['Inventorymaster'];
                        $model->user_id=Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('admin'/*,'id'=>$model->id_inventory*/));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Inventorymaster']))
		{
			$model->attributes=$_POST['Inventorymaster'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
             if (!isset(Yii::app()->session["lastInventory"])) {
            Yii::app()->session["lastInventory"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastInventory"];
        } else {
            Yii::app()->session["lastInventory"] = $_GET;
        }
        
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Inventorymaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            if (!isset(Yii::app()->session["lastInventory"])) {
            Yii::app()->session["lastInventory"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastInventory"];
        } else {
            Yii::app()->session["lastInventory"] = $_GET;
        }

        $model=new Inventorymaster('search');
		$model->unsetAttributes();  // clear any default values
                
                //$model=  Inventorymaster::model()->getNotInTranheader('68');
		if(isset($_GET['Inventorymaster']))
			$model->attributes=$_GET['Inventorymaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Inventorymaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='inventorymaster-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionReportWIP()
        {
            $report= Inventorymaster::model()->getReportWIP();
            
            if(isset($_POST) && count($_POST)>0){
            $date1=$_POST['date1'];
            $date2=$_POST['date2'];
            $report= Inventorymaster::model()->getReportWIP();
            $this->render('reportWIP',array(
			'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));

            }else {

            //$report=new Inventorymaster;
                $date1=date('Y-m-d');
                $date2=date('Y-m-d');
                
                $report= Inventorymaster::model()->getReportWIP();
                $this->render('reportWIP',array(
			'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));
            }
            
        }
        
        public function actionLoadBarang2()
        {
           $data=  Inventorymaster::model()->findAll('code=:Trandetail_kode_barang2', 
           array(':Trandetail_kode_barang2'=> $_POST['Trandetail_kode_barang2']));

           $data=CHtml::listData($data,'id_inventory','description');

           echo "<option value=''>-- Pilih Barang --</option>";
           foreach($data as $value=>$description)
           echo CHtml::tag('option', array('value'=>$value),CHtml::encode($description),true);
        }
        
        public function actionLoadBarang()
        {
         //  $data=  Inventorymaster::model()->findAll('code=:Trandetail_kode_barang', 
           //array(':Trandetail_kode_barang'=> $_POST['Trandetail_kode_barang']));
           $code=$_POST['Trandetail_kode_barang'];
           $data=new CActiveDataProvider('Inventorymaster',array(
            'criteria'=>array(
                'condition'=>"code='$code'",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            )
        ));
           
           //$data2=  Inventorymaster::model()->findAll('code=:Trandetail_kode_barang', 
           //array(':Trandetail_kode_barang'=> $_POST['Trandetail_kode_barang']));

           //$data=CHtml::listData($data,'description','description');
           //$data2=CHtml::listData($data2,'id_inventory','id_inventory');
           
           
           /*echo "<option value=''>-- Pilih Barang --</option>";
           for($i=0, $count = count($data);$i<$count;$i++) {
                echo CHtml::tag('option', array('value'=>$data1['description']),CHtml::encode($data1['description']),true);
               }
            * 
            */

           echo "<option value='' id_inv='' satuan='' tipe=''>-- Pilih Barang --</option>";
           foreach($data->getData() as $i=>$ii){
               //foreach ($data2 as $value2=>$id_inventory){
           echo CHtml::tag('option', array('value'=>$ii['description'], 'id_inv'=>$ii['id_inventory'], 'satuan'=>$ii['satuan'], 'tipe'=>$ii['type']),CHtml::encode($ii['description']),true);
           //break;
             //  }
           }
            
        }
        
        public function actionLaporanWipPDF(){

            if(isset($_POST) && count($_POST)>0){
            $date1=$_POST['date1'];
            $date2=$_POST['date2'];

            $this->renderpartial('laporanWipPDF',array(
			//'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));

            } else{
                $this->renderpartial('laporanWipPDF',array(
			//'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));
            }
        }
        
        public function actionResetWIPReport()
        {
            $this->redirect('reportWIP');
            
        }
}

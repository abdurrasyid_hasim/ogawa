<?php

class TrandetailController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(''),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'transactionAdd', 'create', 'update'),
                //'users'=>array('admin'),
                'expression' => '$user->getRole()<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id) {
        /*if (!isset(Yii::app()->session["lastTransaksi"])) {
            Yii::app()->session["lastTransaksi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastTransaksi"];
        } else {
            Yii::app()->session["lastTransaksi"] = $_GET;
           // $Trandetail_page=Yii::app()->session["lastTransaksi"];
        }*/
        
        $model = new Trandetail;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Trandetail'])) {
            $model->attributes = $_POST['Trandetail'];
            //$str=str_replace(",",".",$model->quantity);
            //$model->quantity=$str;
            $model->tranheader_id = $id;
            //$model->satuan = 'PCS';
            if ($model->save())
                $this->redirect(array('transactionAdd', 'id' => $id, 'Trandetail_page'=>99999999));
        }

        $this->render('create', array(
            'model' => $model,
            'id' => $id,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $idTranHeader) {
        /*if (!isset(Yii::app()->session["lastTransaksi"])) {
            Yii::app()->session["lastTransaksi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastTransaksi"];
        } else {
            Yii::app()->session["lastTransaksi"] = $_GET;
           // $Trandetail_page=Yii::app()->session["lastTransaksi"];
        }*/
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Trandetail'])) {
            $model->attributes = $_POST['Trandetail'];
            //$model->tran
            if ($model->save())
                $this->redirect(array('transactionAdd', 'id' => $idTranHeader));
        }

        $this->render('update', array(
            'model' => $model,
            'id' => $idTranHeader,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $model = $this->loadModel($id);
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                //$this->redirect(array('transactionAdd','id'=>$this->loadModel($id)->tranheader_id));
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('transactionAdd', 'id' => $model->tranheader_id));
            }
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Trandetail');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($id) {
       /*if (!isset(Yii::app()->session["lastTransaksi"])) {
            Yii::app()->session["lastTransaksi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastTransaksi"];
        } else {
            Yii::app()->session["lastTransaksi"] = $_GET;
        }*/
        
        $model = new Trandetail('search');
        $model->unsetAttributes();  // clear any default values
        //$model = new CActiveDataProvider('Trandetail', array(
        //    'criteria' => array(
          //      'condition' => 'tranheader_id=' . $id,
            //),
            //'pagination' => array(
            //    'pageSize' => 10,
            //),
       // ));
        
        if (isset($_GET['Trandetail']))
            $model->attributes = $_GET['Trandetail'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Trandetail::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'trandetail-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionTransactionAdd($id) {
        if (!isset(Yii::app()->session["lastTransaksi"])) {
            Yii::app()->session["lastTransaksi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastTransaksi"];
        } else {
            Yii::app()->session["lastTransaksi"] = $_GET;
           // $Trandetail_page=Yii::app()->session["lastTransaksi"];
        }
        //$id=95;
        //$trandetail = new Trandetail('search');
        //$trandetail->unsetAttributes();  // clear any default values
        
        $tranheader = Tranheader::model()->findByAttributes(array('tranheader_id' => $id));
        /*$trandetail = new CActiveDataProvider('Trandetail', array(
            'criteria' => array(
                'condition' => 'tranheader_id=' . $id,
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));*/   //$trandetail->unsetAttributes(); 
        //$trandetail = new Trandetail("search2('.$id.')");
        $trandetail = new Trandetail("search");
        $trandetail->unsetAttributes(); 

        $this->render('transactionAdd', array(
            'tranheader' => $tranheader,
            'trandetail' => $trandetail,
            //'Trandetail_page'=>$Trandetail_page,
        ));
    }

}

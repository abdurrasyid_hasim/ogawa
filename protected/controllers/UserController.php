<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				//'users'=>array('*'),
                                'expression'=>'$user->getRole()<=1',

			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','index','create','update','changePass'),
				//'users'=>array('admin'),
                                'expression'=>'$user->getRole()<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			
			$dua=$model->password;
			$model->saltPassword=$model->generateSalt();
			$model->password=$model->hashPassword($dua,$model->saltPassword);
			//$model->role=2;			
			//$sss;
			
			/*if(strlen(trim(CUploadedFile::getInstance($model,'avatar'))) > 0)
			{
				$sss=CUploadedFile::getInstance($model,'avatar');
				$model->avatar=$model->username.'.'.$sss->extensionName;
			}*/
/*			
                        try {
if($model->save())
        $this->redirect(array('view','id'=>$model->ID));
}
catch(CDbException $e) {
        $model->addError(null, $e->getMessage());
}*/
			if($model->save())
			{
				/*if(strlen(trim($model->avatar)) > 0)
				{			
					$sss->saveAs(Yii::app()->basePath . '/../avatar/' . $model->avatar);
				}*/
				
				//$model2=new LoginForm;
				
				//$model2->username=$model->user_name;
				//$model2->password=$dua;
				
				//if($model2->login())
					$this->redirect(array('user/admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                //$model->password="";
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
                        //$dua=$model->password;
			//$model->saltPassword=$model->generateSalt();
			//$model->password=$model->hashPassword($dua,$model->saltPassword);
			//$model->role=2;
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
                //$model=new CActiveDataProvider('User');
		
                if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionChangePass($id)
        {
                $model=$this->loadModel($id);
                //$model->password="";
            //$model->setScenario('changePwd');
                

		if(isset($_POST) && count($_POST)>0)
		{
			//$model->attributes=$_POST['new_password'];
                        
                        $newpass=$_POST['new_password'];
			$model->saltPassword=$model->generateSalt();
			$model->password=$model->hashPassword($newpass,$model->saltPassword);
                        
                        
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('changePass',array(
			'model'=>$model,
		));
            
            
        }
}

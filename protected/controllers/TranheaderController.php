<?php

class TranheaderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('ReportMutasiFG','ReportMutasiSR','ReportMutasiRM','reportMutasiMP','mutasiRmPDF','mutasiFgPDF','mutasiMpPDF','mutasiSrPDF','ResetFG','ResetSR','ResetRM','ResetMP'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','transaction', 'viewTransaction','createNewTransaction','transactionAdd','process'),
				//'users'=>array('admin'),
                             'expression'=>'$user->getRole()<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new Tranheader;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tranheader']))
		{
			$model->attributes=$_POST['Tranheader'];
                        $model->user_id=Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('view','id'=>$model->tranheader_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            $tranheader= $this->loadModel($id);
            $trandetail=new CActiveDataProvider('Trandetail', array(
                'criteria'=>array(
                    'condition'=>'tranheader_id='.$id,
                     ),
                ));		//$trandetail->unsetAttributes();  
                
           $this->render('//trandetail/transactionAdd',array(
			'tranheader'=>$tranheader,
                        'trandetail'=>$trandetail,
		));
		/*$tranheader=$this->loadModel($id);
                $inventoryMaster=new CActiveDataProvider('Inventorymaster');
                $trandetail=new CActiveDataProvider('Trandetail', array(
                'criteria'=>array(
                    'condition'=>'tranheader_id='.$id,
                     ),
                ));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tranheader']))
		{
			$model->attributes=$_POST['Tranheader'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->tranheader_id));
		}

		$this->render('transactionUpdate',array(
                        'inventoryMaster'=>$inventoryMaster,
			'tranheader'=>$tranheader,
                        'trandetail'=>$trandetail,
		));*/
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Tranheader');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            if (!isset(Yii::app()->session["lastMutasi"])) {
            Yii::app()->session["lastMutasi"] = array();
        }

        if (empty($_GET)) {
            $_GET = Yii::app()->session["lastMutasi"];
        } else {
            Yii::app()->session["lastMutasi"] = $_GET;
           // $Trandetail_page=Yii::app()->session["lastTransaksi"];
        }
		$model=new Tranheader('search');
		$model->unsetAttributes();  // clear any default values
                
                /*$tranheader = new CActiveDataProvider('Tranheader', array(
            'criteria' => array(
               // 'condition' => 'tranheader_id=' . $id,
                'order'=>'tranheader_id DESC',
            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));*/
		if(isset($_GET['Tranheader']))
			$model->attributes=$_GET['Tranheader'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Tranheader::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tranheader-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        
        public function actionTransaction()
        {
            $models=new CActiveDataProvider('Inventorymaster');
            
            if(isset($_POST) && count($_POST)>0)
            {
            //$this->saveTransaction($_POST);
                
                $tranHeader = new Tranheader;
                //$tranHeader->attributes=$_POST['Tranheader'];
                $tranHeader->tanggal_tran= $_POST['Tranheader']['tanggal_tran'];
                $tranHeader->status_tran=$_POST['Tranheader']['status_tran'];
                $tranHeader->type_tran=$_POST['Tranheader']['type_tran'];
                $tranHeader->user_id=Yii::app()->user->id;
                //$tranHeader->tranheader_id=1;
                if($tranHeader->validate()){
                $tranHeader->save();
                }
                
                $count = count($_POST['Trandetail']['quantity']);// hitung jumlah transaksi
                $idTranHeader=$tranHeader->tranheader_id;

                for($x=0;$x<$count;$x++) //looping transaksi
                {
                    $trandetail = new Trandetail();
                    $trandetail->tranheader_id= $idTranHeader;
                    $trandetail->kode_barang = $_POST['Trandetail']['kode_barang'][$x];
                    $trandetail->nama_barang = $_POST['Trandetail']['nama_barang'][$x];
                    $trandetail->quantity = $_POST['Trandetail']['quantity'][$x];
                    $trandetail->satuan = $_POST['Trandetail']['satuan'][$x];
                    
                    if($trandetail->quantity==NULL)$trandetail->quantity='1';
                    
                    if($trandetail->validate()){
                    $trandetail->save();
                    }
                }
                $this->redirect(array('viewTransaction','id'=>$tranHeader->tranheader_id));
                
            }  

            $this->render('transaction',
               array('models'=>$models)
               );
        }
        
       public function actionViewTransaction($id){
           $tranheader=$this->loadModel($id);
            $trandetail=new CActiveDataProvider('Trandetail', array(
                'criteria'=>array(
                    'condition'=>'tranheader_id='.$id,
                     ),
                ));		//$trandetail->unsetAttributes();  
                
           $this->render('viewTransaction',array(
			'tranheader'=>$tranheader,
                        'trandetail'=>$trandetail,
		));
           
           
       }
       
       public function actionCreateNewTransaction()
       {
           $results = Tranheader::model()->findByAttributes(array('status_tran'=>'N')); //count where N
           $count = count ($results);
           
           if($count>0)
           {
               throw new CHttpException(' :There are Unprocessed Transactions');
           }
           
           $model=new Tranheader;
           
           if(isset($_POST['Tranheader']))
		{
			$model->attributes=$_POST['Tranheader'];
                        $model->user_id=Yii::app()->user->id;
			if($model->save())
				$this->redirect(array('//trandetail/transactionAdd','id'=>$model->tranheader_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
           
           
       }
       
       /*public function actionTransactionAdd($id)
       {
           $tranheader=$this->loadModel($id);
            $trandetail=new CActiveDataProvider('Trandetail', array(
                'criteria'=>array(
                    'condition'=>'tranheader_id='.$id,
                     ),
                ));		//$trandetail->unsetAttributes();  
                
           $this->render('//trandetail/transactionAdd',array(
			'tranheader'=>$tranheader,
                        'trandetail'=>$trandetail,
		));
           
       }*/
       
       public function actionProcess($id)
       {
           //$tranheader= $tranheader=$this->loadModel($id);
            
           $command = Yii::app()->db->createCommand("call process($id)");
           if($command->execute()){
               $this->redirect(array('//trandetail/transactionAdd','id'=>$id));
               
           }
           else{
               $this->redirect(array('//trandetail/transactionAdd','id'=>$id));
               
           }
  
       }
       
       public function actionReportMutasiFG()
       {
           $this->layout='//layouts/main'; 
           
           if(isset(Yii::app()->request->cookies['FGcdate1']->value)){
        $date1=Yii::app()->request->cookies['FGcdate1']->value;
        $date2=Yii::app()->request->cookies['FGcdate2']->value;
        $report_id=Yii::app()->request->cookies['FGrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
           
           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasifg',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));
           
		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
            
            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;
            
            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiFG('$report_id','$date1','$date2')");
            $command->execute();
            
            Yii::app()->request->cookies['FGcdate1'] = new CHttpCookie('FGcdate1', $date1);
            Yii::app()->request->cookies['FGcdate2'] = new CHttpCookie('FGcdate2', $date2);
            Yii::app()->request->cookies['FGrptId'] = new CHttpCookie('FGrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasifg',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'bpb_tanggal ASC',
            )
        ));
        $this->render('reportMutasiFG', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportMutasiFG', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
  
       }
       
       public function actionMutasiFgPDF(){
           $this->layout='//layouts/main';

           if(isset(Yii::app()->request->cookies['FGcdate1']->value)){
        $date1=Yii::app()->request->cookies['FGcdate1']->value;
        $date2=Yii::app()->request->cookies['FGcdate2']->value;
        $report_id=Yii::app()->request->cookies['FGrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }

           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasifg',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));

		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));

            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;

            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiFG('$report_id','$date1','$date2')");
            $command->execute();

            Yii::app()->request->cookies['FGcdate1'] = new CHttpCookie('FGcdate1', $date1);
            Yii::app()->request->cookies['FGcdate2'] = new CHttpCookie('FGcdate2', $date2);
            Yii::app()->request->cookies['FGrptId'] = new CHttpCookie('FGrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasifg',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            ),
            'pagination'=>array(
        'pageSize'=>20000,),
        ));
        $this->renderpartial('mutasiFgPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{

        $this->renderpartial('mutasiFgPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
       }
       
       
       public function actionReportMutasiSR()
       {
           $this->layout='//layouts/main'; 
           
           if(isset(Yii::app()->request->cookies['SRcdate1']->value)){
        $date1=Yii::app()->request->cookies['SRcdate1']->value;
        $date2=Yii::app()->request->cookies['SRcdate2']->value;
        $report_id=Yii::app()->request->cookies['SRrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
        //$report_id=0;
        
           
           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasisr',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));
           
		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
                    
            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;
            
            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiSR($report_id,'$date1','$date2')");
            $command->execute();
            
            Yii::app()->request->cookies['SRcdate1'] = new CHttpCookie('SRcdate1', $date1);
            Yii::app()->request->cookies['SRcdate2'] = new CHttpCookie('SRcdate2', $date2);
            Yii::app()->request->cookies['SRrptId'] = new CHttpCookie('SRrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasisr',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            )
        ));
        $this->render('reportMutasiSR', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportMutasiSR', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
  
       }
       
       public function actionMutasiSrPDF(){
              $this->layout='//layouts/main';

           if(isset(Yii::app()->request->cookies['SRcdate1']->value)){
        $date1=Yii::app()->request->cookies['SRcdate1']->value;
        $date2=Yii::app()->request->cookies['SRcdate2']->value;
        $report_id=Yii::app()->request->cookies['SRrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
        //$report_id=0;


           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasisr',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));

		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));

            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;

            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiSR($report_id,'$date1','$date2')");
            $command->execute();

            Yii::app()->request->cookies['SRcdate1'] = new CHttpCookie('SRcdate1', $date1);
            Yii::app()->request->cookies['SRcdate2'] = new CHttpCookie('SRcdate2', $date2);
            Yii::app()->request->cookies['SRrptId'] = new CHttpCookie('SRrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasisr',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            ),
            'pagination'=>array(
        'pageSize'=>20000,),
        ));
        $this->renderpartial('mutasiSrPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{

        $this->renderpartial('mutasiSrPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
       }
       
       public function actionReportMutasiRM()
       {
           $this->layout='//layouts/main'; 
           
           if(isset(Yii::app()->request->cookies['RMcdate1']->value)){
        $date1=Yii::app()->request->cookies['RMcdate1']->value;
        $date2=Yii::app()->request->cookies['RMcdate2']->value;
        $report_id=Yii::app()->request->cookies['RMrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
        //$report_id=1;
           
           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasirm',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));
           
		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
            
             //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;
            
            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiRM($report_id,'$date1','$date2')");
            $command->execute();
            
            Yii::app()->request->cookies['RMcdate1'] = new CHttpCookie('RMcdate1', $date1);
            Yii::app()->request->cookies['RMcdate2'] = new CHttpCookie('RMcdate2', $date2);
             Yii::app()->request->cookies['RMrptId'] = new CHttpCookie('RMrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasirm',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            )
        ));
        $this->render('reportMutasiRM', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportMutasiRM', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
  
       }
       
       public function actionMutasiRmPDF(){
              $this->layout='//layouts/main';

           if(isset(Yii::app()->request->cookies['RMcdate1']->value)){
        $date1=Yii::app()->request->cookies['RMcdate1']->value;
        $date2=Yii::app()->request->cookies['RMcdate2']->value;
        $report_id=Yii::app()->request->cookies['RMrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
        //$report_id=1;

           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasirm',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));

		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));

             //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;

            //call procedure
            $command = Yii::app()->db->createCommand("call rptmutasiRM($report_id,'$date1','$date2')");
            $command->execute();

            Yii::app()->request->cookies['RMcdate1'] = new CHttpCookie('RMcdate1', $date1);
            Yii::app()->request->cookies['RMcdate2'] = new CHttpCookie('RMcdate2', $date2);
             Yii::app()->request->cookies['RMrptId'] = new CHttpCookie('RMrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasirm',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            ),
            'pagination'=>array(
        'pageSize'=>20000,),
        ));
        $this->renderpartial('mutasiRmPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{

        $this->renderpartial('mutasiRmPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
       }
       
       public function actionReportMutasiMP()
       {
           $this->layout='//layouts/main'; 
           
           if(isset(Yii::app()->request->cookies['MPcdate1']->value)){
        $date1=Yii::app()->request->cookies['MPcdate1']->value;
        $date2=Yii::app()->request->cookies['MPcdate2']->value;
        $report_id=Yii::app()->request->cookies['MPrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }
           
           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasimp',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));
           
		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
            
            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;
            
            
            $command = Yii::app()->db->createCommand("call rptmutasiMP($report_id,'$date1','$date2')");
            $command->execute();
            
            Yii::app()->request->cookies['MPcdate1'] = new CHttpCookie('MPcdate1', $date1);
            Yii::app()->request->cookies['MPcdate2'] = new CHttpCookie('MPcdate2', $date2);
            Yii::app()->request->cookies['MPrptId'] = new CHttpCookie('MPrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasimp',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            )
        ));
        $this->render('reportMutasiMP', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportMutasiMP', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
  
       }
       
       public function actionMutasiMpPDF(){
           $this->layout='//layouts/main';

           if(isset(Yii::app()->request->cookies['MPcdate1']->value)){
        $date1=Yii::app()->request->cookies['MPcdate1']->value;
        $date2=Yii::app()->request->cookies['MPcdate2']->value;
        $report_id=Yii::app()->request->cookies['MPrptId']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
            $report_id=0;
        }

           //$model=new Verptmutasifg('search');
		//$model->unsetAttributes();  // clear any default values
           $report=new CActiveDataProvider('Verptmutasimp',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));

		if(isset($_POST) && count($_POST)>0){
            $date1=date('Y-m-d', strtotime($_POST['date1']));
            $date2=date('Y-m-d', strtotime($_POST['date2']));
            
            //$date1 = date('Y-m-d', $date1);
            //$date2 = date('Y-m-d', $date2);

            //generate reportid
            $a=date('His');
            $b=rand(1000,1);
            //$c=Yii::app()->user->name;
            $x=$a.$b;
            //echo $x;
            $report_id=$x;


            $command = Yii::app()->db->createCommand("call rptmutasiMP($report_id,'$date1','$date2')");
            $command->execute();

            Yii::app()->request->cookies['MPcdate1'] = new CHttpCookie('MPcdate1', $date1);
            Yii::app()->request->cookies['MPcdate2'] = new CHttpCookie('MPcdate2', $date2);
            Yii::app()->request->cookies['MPrptId'] = new CHttpCookie('MPrptId', $report_id);
        $report=new CActiveDataProvider('Verptmutasimp',array(
            'criteria'=>array(
                'condition'=>"report_id=$report_id",
                //'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                //'order'=>'vehicle_id DESC',
            ),
            'pagination'=>array(
        'pageSize'=>20000,),
        ));
        $this->renderpartial('mutasiMpPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{

        $this->renderpartial('mutasiMpPDF', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
       }
       
       public function actionResetFG()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['FGcdate1']);
            unset(Yii::app()->request->cookies['FGcdate2']);
            $this->redirect('reportMutasiFG');
        }
        
        public function actionResetSR()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['SRcdate1']);
            unset(Yii::app()->request->cookies['SRcdate2']);
            $this->redirect('reportMutasiSR');
        }
        
        public function actionResetRM()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['RMcdate1']);
            unset(Yii::app()->request->cookies['RMcdate2']);
            $this->redirect('reportMutasiRM');
        }
        
        public function actionResetMP()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['MPcdate1']);
            unset(Yii::app()->request->cookies['MPcdate2']);
            $this->redirect('reportMutasiMP');
        }
}

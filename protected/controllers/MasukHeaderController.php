<?php

class MasukHeaderController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('ReportMasukPabean','MasukPabeanPDF','ResetMasukReport'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'tampilxml', 'hasil','create', 'update'),
                //'users'=>array('admin'),
                'expression' => '$user->getRole()<=1',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new MasukHeader;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MasukHeader'])) {
            $model->attributes = $_POST['MasukHeader'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id_masuk_header));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MasukHeader'])) {
            $model->attributes = $_POST['MasukHeader'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id_masuk_header));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('MasukHeader');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new MasukHeader('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MasukHeader']))
            $model->attributes = $_GET['MasukHeader'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = MasukHeader::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'masuk-header-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionTampilxml() {
        $model = new MasukHeader;
        $msg = '0';

        if (isset($_POST['MasukHeader'])) {
            $model->attributes = $_POST['MasukHeader'];
            $itu = CUploadedFile::getInstance($model, 'filee');
            if ($itu != NULL) {

                $path = Yii::app()->basePath . '/../jajal.xml';
                $itu->saveAs($path);

                $fileinfo = pathinfo($itu);
                if ($fileinfo['extension'] != 'xml') {
                    throw new CHttpException(' :File extension must be XML');
                }

                $xml = simplexml_load_file($path);

                foreach ($xml->children() as $child) {

                    $masukHeader = new MasukHeader;
                    $masukHeader->bpb_nomor = $child->BPBNomor;
                    
                    //format tanggal
                    $dateform= new DateTime($child->BPBTanggal); 
                    $masukHeader->bpb_tanggal = $dateform->format('Y-m-d H:i:s');
                    
                    $masukHeader->dp_jenis = $child->DPJenis;
                    $masukHeader->dp_nomor = $child->DPNomor;
                    
                    //format tanggal
                    $dateform= new DateTime($child->DPTanggal);
                    $masukHeader->dp_tanggal = $dateform->format('Y-m-d H:i:s');
                    $masukHeader->pengirim = $child->Pengirim;

                    if ($masukHeader->validate()) {
                        $masukHeader->save();
                        // $this->redirect('hasil');
                    } else {
                        echo "terjadi kesalahan pemasukan data pada HEADER. Mohon dicek kelengkapan data dan tipe datanya";
                    }

                    $masukHeaderId = $masukHeader->id_masuk_header;
                    foreach ($child->children() as $grandchild) {

                        if ($grandchild->getName() == 'Detail') {
                            //echo $grandchild->getName() . ": " . $grandchild->KodeBarang . " | " . $grandchild->NamaBarang ." | " . $grandchild->Jumlah . " | " . $grandchild->Satuan . " | " . $grandchild->Nilai ."<br>";
                            $masukDetail = new MasukDetail();
                            $masukDetail->id_masuk_header = $masukHeaderId;
                            $masukDetail->jumlah = floatval($grandchild->Jumlah);
                            $masukDetail->kode_barang = $grandchild->KodeBarang;
                            $masukDetail->nama_barang = $grandchild->NamaBarang;
                            $masukDetail->nilai = floatval($grandchild->Nilai);
                            $masukDetail->satuan = $grandchild->Satuan;

                            if ($masukDetail->validate()) {
                                $masukDetail->save();
                                
                            } else {
                                echo "terjadi kesalahan pemasukan data pada DETAIL. Mohon dicek kelengkapan data dan tipe datanya";
                            }
                        }
                    }
                }


                unlink($path);
                $msg = '1';
                //$this->redirect('admin');
                $this->redirect(array('tampilxml', 'msg' => $msg));
            }
        }

        $this->render('tampilxml', array('model' => $model, 'msg' => $msg));
    }

    public function actionHasil($id) {
        $masukHeader = $this->loadModel($id);
        $masukDetail = new CActiveDataProvider('MasukDetail', array(
            'criteria' => array(
                'condition' => 'id_masuk_header=' . $id,
            ),
        ));  //$trandetail->unsetAttributes();

        $this->render('hasil', array(
            'masukHeader' => $masukHeader,
            'masukDetail' => $masukDetail,
        ));
    }
    
    public function actionReportMasukPabean()
        {
        $this->layout='//layouts/main';
            //$model = new MasukHeader('search');
        //$model->unsetAttributes();  // clear any default values
        if(isset(Yii::app()->request->cookies['cdate1']->value)){
        $date1=Yii::app()->request->cookies['cdate1']->value;
        $date2=Yii::app()->request->cookies['cdate2']->value;
        }else{
            $date1=date('Y-m-d');
            $date2=date('Y-m-d');
        }
        
        /*$report=new CActiveDataProvider('masukdetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));*/
        $report=new CActiveDataProvider('Vmasuk', array(
            'sort'=>array(
    'defaultOrder'=>'bpb_tanggal ASC',
  ),
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                //'order'=>'bpb_tanggal ASC',
            ),
        ));
        
        if(isset($_POST) && count($_POST)>0){
            $date1= date('Y-m-d', strtotime($_POST['date1']));
            $date2= date('Y-m-d', strtotime($_POST['date2']));
            
            //$date1 = Yii::app()->dateFormatter->format("m/d/Y",strtotime($datep1));
            //$date2 = Yii::app()->dateFormatter->format("m/d/Y",strtotime($datep2));
            
            Yii::app()->request->cookies['cdate1'] = new CHttpCookie('cdate1',$date1);
            Yii::app()->request->cookies['cdate2'] = new CHttpCookie('cdate2',$date2);
        /*$report=new CActiveDataProvider('masukdetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                'order'=>'bpb_tanggal ASC',
            )
        ));*/
        $report=new CActiveDataProvider('Vmasuk', array(
            'sort'=>array(
    'defaultOrder'=>'bpb_tanggal ASC',
  ),
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                //'order'=>'bpb_tanggal ASC',
            ),
        ));
        
        //echo $date1;
        $this->render('reportMasukPabean', array(
            'date1'=>$_POST['date1'],
            'date2'=>$_POST['date2'],
            'model' => $report,
        ));
        //if (isset($_GET['MasukHeader']))
          //  $model->attributes = $_GET['MasukHeader'];
        }else{
            
        $this->render('reportMasukPabean', array(
            'date1'=>$date1,
            'date2'=>$date2,
            'model' => $report,
        ));
        }
        
    }
    
    public function actionMasukPabeanPDF()
            {
                
                /*if(isset(Yii::app()->request->cookies['cdate1']->value)){
        $date1=Yii::app()->request->cookies['cdate1']->value;
        $date2=Yii::app()->request->cookies['cdate2']->value;
        }else{*/
             $date1=date('Y-m-d');
            $date2=date('Y-m-d');
        //}
        
        $report=new CActiveDataProvider('masukdetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                )));

            if(isset($_POST) && count($_POST)>0){
                $date1=date('Y-m-d', strtotime($_POST['date1']));
                $date2=date('Y-m-d', strtotime($_POST['date2']));

                $criteria= new CDbCriteria;
                $criteria->addBetweenCondition('bpb_tanggal', $date1, $date2);

                $report=new CActiveDataProvider('masukdetail',array(
            'criteria'=>array(
                'condition'=>"bpb_tanggal between '$date1' and '$date2'",
                'with'=>array('idMasukHeader'=>array('joinType'=>'LEFT JOIN')),
                'order'=>'bpb_tanggal ASC',
            ),
                    'pagination'=>array(
        'pageSize'=>20000,
    ),
        ));
               // $reportMasuk= MasukHeader::model()->getJoinDetail($date1,$date2);
      
             $this->renderPartial('masukPabeanPDF',array(
			'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));
            }
            else{
            $date1=NULL;
            $date2=NULL;

            $this->renderPartial('masukPabeanPDF',array(
			'model'=>$report,
                        'date1'=>$date1,
                        'date2'=>$date2,
		));
            }
        }
        
        public function actionResetMasukReport()
        {
            //unset(Yii::app()->request->cookies['cookie_name']);
            unset(Yii::app()->request->cookies['cdate1']);
            unset(Yii::app()->request->cookies['cdate2']);
            $this->redirect('reportMasukPabean');
        }
        

}

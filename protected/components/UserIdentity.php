<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
       const ERROR_USERNAME_INACTIVE = 3;
       private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
                $username=  strtolower($this->username);
                
                if(strpos($this->username, '@') !== false){
                $user = User::model()->findByAttributes(array('email'=>$this->username));
                }else{
                //Otherwise we search using the username
                $user = User::model()->findByAttributes(array('user_name'=>$this->username));
                }
                
                if($user==null)
                    $this->errorCode=self::ERROR_USERNAME_INVALID;
                else if(!$user->validatePassword($this->password))
                    $this->errorCode=  self::ERROR_PASSWORD_INVALID;
                else if($user->status=='I')
                    $this->errorCode= self::ERROR_USERNAME_INACTIVE;
                else
                {
                    $this->_id=$user->user_id;
                    $this->username = $user->user_name;
                    $this->errorCode = self::ERROR_NONE;           
                }
                return $this->errorCode == self::ERROR_NONE;
                
                
                /*$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;*/
                 
	}
        
        public function getId()
        {
            return $this->_id;    
        }
}
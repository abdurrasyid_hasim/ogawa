<?php
class UserActivityLog {

	private $message;
	private $category 	= '';
	private $levelInfo 	= CLogger::LEVEL_INFO;
	private $action;
	private $extras;

	private $session_NameSurname;
	private $session_UserId;
	private $session_UserEmail;

	private $current_Module;
	private $current_Controller;
	private $current_Function;

	private $logType = 'ogawa'; // db, file


	function __construct($user = NULL)
	{
		$userData = $this->_setUserData($user);

		if(FALSE === $userData)
		{
			$this->isAdd = FALSE;
		}
		else 
		{
			$this->isAdd = TRUE;

			$this->session_UserId 		= 	$userData->user_id;
			$this->session_NameSurname 	= 	$userData->name_surname;
			$this->session_UserEmail 	= 	$userData->email;
		}		
	}

	public function setLogin($user = NULL) 
	{
		if(FALSE=== $this->isAdd)		
			return;

		$this->_setPageData();

		$this->message 		= 	$this->session_NameSurname .' login to user panel.';
		$this->category 	= 	$this->_getCategory();
		$this->levelInfo 	= 	CLogger::LEVEL_INFO;
		$this->action 		= 	'login';

		$this->_log();
	}

	public function setLogout($user = NULL) 
	{
		if(FALSE === $this->isAdd)		
			return;

		$this->_setPageData();

		$this->message 		= 	$this->session_NameSurname .' logout from user panel.';
		$this->category 	= 	$this->_getCategory();
		$this->levelInfo 	= 	CLogger::LEVEL_INFO;
		$this->action 		= 	'logout';
		
		$this->_log();
	}

	public function setUpdateSettings($extras)
	{
		if(FALSE === $this->isAdd)		
			return;

		$this->_setPageData();

		$this->message 		= 	$this->session_NameSurname .' changed info.';
		$this->category 	= 	$this->_getCategory();
		$this->levelInfo 	= 	CLogger::LEVEL_INFO;
		$this->action 		= 	'update_settings';
		$this->extras 		= 	$extras;

		$this->_log();
	}

	private function _getCategory()
	{
		$category = $this->current_Module . ' > ' . $this->current_Controller . ' > ' . $this->current_Function;

		return $category;
	}

	private function _log()
	{
		if('' == $this->message)
			return;

		switch ($this->logType) {
			case 'db':
				$this->_addDb();
				break;
			
			case 'file':
				$this->_addFile();
				break;
			
			default:
				$this->_addDb();
				break;
		}

	}

	private function _addDb()
	{
		$userLog = new LogUser;

		$userLog->user_id 		= 	$this->session_UserId;
		$userLog->action 		= 	$this->action;
		$userLog->message 		= 	$this->message;
		$userLog->module 		= 	$this->current_Module;
		$userLog->controller 		= 	$this->current_Controller;
		$userLog->function 		= 	$this->current_Function;

		if('' != $this->extras)
			$userLog->extras = serialize($this->extras);

		$userLog->save();
	}

	private function _addFile()
	{
		Yii::log($this->message, $this->levelInfo, $this->category);	
	}

	private function _setUserData($user)
	{
		if($user !== NULL)
			return $user;

		if(! isset(Yii::app()->params['session']['user_id']))
			return FALSE;

		$userId = Yii::app()->params['session']['user_id'];

		$user = User::model()->findByPk($userId);		

		if(isset($user->user_id))
			return FALSE;

		return $user;
	}

	private function _setPageData()
	{
		$this->current_Module 		= 	Yii::app()->controller->module->id;
		$this->current_Controller 	= 	Yii::app()->controller->id;
		$this->current_Function 	= 	Yii::app()->controller->action->id;
	}
}


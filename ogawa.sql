-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 05. September 2014 jam 12:31
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ogawa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventorymaster`
--

CREATE TABLE IF NOT EXISTS `inventorymaster` (
  `id_inventory` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  `inventory` text NOT NULL,
  `description` text NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `status` char(1) NOT NULL,
  `user_id` int(3) NOT NULL,
  PRIMARY KEY (`id_inventory`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_inventorymaster_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `inventorymaster`
--

INSERT INTO `inventorymaster` (`id_inventory`, `code`, `inventory`, `description`, `satuan`, `status`, `user_id`) VALUES
(1, 'A222', 'bedak', 'bedak apapun', 'kg', 'A', 2),
(3, 'A221', 'aasdas', 'asj;fsa;jbfasjb', 'KG', '0', 2),
(4, 'A234', 'ahsflah', 'abf;khasvph', 'KG', 'A', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL,
  `user_id` int(3) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `FK_log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `log`
--

INSERT INTO `log` (`log_id`, `activity`, `datetime`, `user_id`) VALUES
(10, 'logout', '2014-09-05 12:04:57', 2),
(11, 'Login', '2014-09-05 12:05:12', 2),
(12, 'logout', '2014-09-05 12:05:50', 2),
(13, 'Login', '2014-09-05 12:06:10', 2),
(14, 'logout', '2014-09-05 12:09:39', 8),
(15, 'Login', '2014-09-05 12:09:51', 2),
(16, 'logout', '2014-09-05 12:18:00', 9),
(17, 'Login', '2014-09-05 12:18:12', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(60) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `saltPassword` varchar(30) NOT NULL DEFAULT '0',
  `role` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `fullname`, `user_name`, `password`, `saltPassword`, `role`, `status`) VALUES
(2, 'abdurrasyid', 'ocit2', '0c8c3d8342e709046ed866920ec69b80', '5406917b6c8103.79267193', 1, 1),
(7, 'setia', 'fandi', '4c11c4380d91b6190eb37314105880d7', '54069588593ed4.74983572', 2, 1),
(8, 'fandi sastra', 'fandi2', '868ff7f525f592e848153cab500f436a', '540937756d9805.67658051', 2, 1),
(9, 'fandi', 'fandi22', 'da4a26a4b5908b5b9be63789c4eaa41a', '5409395c6f3bd4.36471892', 2, 1),
(10, 'fandi', 'fandi121', '1c554b6cca4c2523e72c59032499b8f0', '540939bd898476.50366412', 2, 1),
(11, 'fandi', 'fnadi', 'b89411b293f5b38edb3c64b0af0b33b5', '540939f832ba72.40220762', 2, 1);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `inventorymaster`
--
ALTER TABLE `inventorymaster`
  ADD CONSTRAINT `FK_inventorymaster_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Ketidakleluasaan untuk tabel `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_log_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

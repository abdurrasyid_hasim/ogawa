-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 16, 2014 at 08:58 AM
-- Server version: 5.5.31
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ogawa`
--
CREATE DATABASE IF NOT EXISTS `ogawa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ogawa`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `process`(IN `tranheader_id_param` INT)
BEGIN
	UPDATE tranheader
		SET status_tran='P'
		WHERE tranheader_id=tranheader_id_param;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inventorymaster`
--

CREATE TABLE IF NOT EXISTS `inventorymaster` (
  `id_inventory` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  `inventory` text NOT NULL,
  `description` text NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `status` char(1) NOT NULL,
  `user_id` int(3) NOT NULL,
  PRIMARY KEY (`id_inventory`),
  UNIQUE KEY `code` (`code`),
  KEY `FK_inventorymaster_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `inventorymaster`
--

INSERT INTO `inventorymaster` (`id_inventory`, `code`, `inventory`, `description`, `satuan`, `status`, `user_id`) VALUES
(1, 'A222', 'bedak', 'bedak apapun', 'METER', 'A', 2),
(3, 'A221', 'aasdas', 'asj;fsa;jbfasjb', 'BUAH', 'I', 2),
(4, 'A234', 'ahsflah', 'abf;khasvph', 'KG', 'A', 2),
(5, 'A333', 'Barang4', 'deskripsi4', 'PCS', 'A', 14),
(6, 'A345', 'exhaust fan', 'deker teesss sdfasdf', 'UNIT', 'A', 14);

-- --------------------------------------------------------

--
-- Table structure for table `keluar_detail`
--

CREATE TABLE IF NOT EXISTS `keluar_detail` (
  `id_keluar_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_keluar_header` int(11) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(60) NOT NULL,
  `jumlah` float NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `nilai` float NOT NULL,
  PRIMARY KEY (`id_keluar_detail`),
  KEY `FK_keluar_detail_keluar_header` (`id_keluar_header`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `keluar_detail`
--

INSERT INTO `keluar_detail` (`id_keluar_detail`, `id_keluar_header`, `kode_barang`, `nama_barang`, `jumlah`, `satuan`, `nilai`) VALUES
(1, 1, 'BARANG01', 'Barang 01', 5, 'KG', 125000000),
(2, 1, 'BARANG02', 'Barang 02', 10, 'LTR', 75000000),
(3, 1, 'BARANG03', 'Barang 03', 20, 'LTR', 175000000),
(4, 2, 'TEST01', 'Test01', 1, 'KG', 100000),
(5, 2, 'TEST02', 'TEST 02', 20, 'LTR', 70000000),
(6, 3, 'BARANG01', 'Barang 01', 5, 'KG', 125000000),
(7, 3, 'BARANG02', 'Barang 02', 10, 'LTR', 75000000),
(8, 3, 'BARANG03', 'Barang 03', 20, 'LTR', 175000000),
(9, 4, 'TEST01', 'Test01', 1, 'KG', 100000),
(10, 4, 'TEST02', 'TEST 02', 20, 'LTR', 70000000);

-- --------------------------------------------------------

--
-- Table structure for table `keluar_header`
--

CREATE TABLE IF NOT EXISTS `keluar_header` (
  `id_keluar_header` int(11) NOT NULL AUTO_INCREMENT,
  `dp_jenis` varchar(10) NOT NULL,
  `dp_nomor` varchar(30) NOT NULL,
  `dp_tanggal` date NOT NULL,
  `bpb_nomor` varchar(30) NOT NULL,
  `bpb_tanggal` date NOT NULL,
  `pengirim` varchar(100) NOT NULL,
  PRIMARY KEY (`id_keluar_header`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `keluar_header`
--

INSERT INTO `keluar_header` (`id_keluar_header`, `dp_jenis`, `dp_nomor`, `dp_tanggal`, `bpb_nomor`, `bpb_tanggal`, `pengirim`) VALUES
(1, 'BC40', '00001/TEST/201409', '2014-09-05', '000124', '2014-09-07', 'PT. ABC'),
(2, 'BC40', '00002/TEST/201409', '2014-09-06', '000125', '2014-09-10', 'PT. XYZ'),
(3, 'BC40', '00001/TEST/201409', '2014-09-05', '000124', '2014-09-07', 'PT. ABC'),
(4, 'BC40', '00002/TEST/201409', '2014-09-06', '000125', '2014-09-10', 'PT. XYZ');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(50) NOT NULL,
  `datetime` datetime NOT NULL,
  `user_id` int(3) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `FK_log_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `activity`, `datetime`, `user_id`) VALUES
(10, 'logout', '2014-09-05 12:04:57', 2),
(11, 'Login', '2014-09-05 12:05:12', 2),
(12, 'logout', '2014-09-05 12:05:50', 2),
(13, 'Login', '2014-09-05 12:06:10', 2),
(14, 'logout', '2014-09-05 12:09:39', 8),
(15, 'Login', '2014-09-05 12:09:51', 2),
(16, 'logout', '2014-09-05 12:18:00', 9),
(17, 'Login', '2014-09-05 12:18:12', 2),
(18, 'Login', '2014-09-08 06:29:06', 2),
(19, 'logout', '2014-09-08 10:18:31', 2),
(20, 'Login', '2014-09-08 10:18:41', 2),
(21, 'logout', '2014-09-08 10:19:19', 2),
(22, 'Login', '2014-09-08 10:19:24', 12),
(23, 'logout', '2014-09-08 10:20:36', 12),
(24, 'Login', '2014-09-08 10:22:02', 2),
(25, 'logout', '2014-09-08 10:22:21', 2),
(26, 'Login', '2014-09-08 10:22:39', 2),
(27, 'logout', '2014-09-08 10:22:59', 2),
(28, 'Login', '2014-09-08 10:23:04', 12),
(29, 'logout', '2014-09-08 10:23:05', 12),
(30, 'Login', '2014-09-08 10:23:12', 2),
(31, 'logout', '2014-09-08 10:23:13', 2),
(32, 'Login', '2014-09-08 10:23:25', 2),
(33, 'logout', '2014-09-08 10:26:29', 2),
(34, 'Login', '2014-09-08 10:26:33', 12),
(35, 'Login', '2014-09-08 10:27:45', 2),
(36, 'logout', '2014-09-08 10:31:33', 2),
(37, 'Login', '2014-09-08 10:31:57', 2),
(38, 'Login', '2014-09-09 11:37:42', 13),
(39, 'logout', '2014-09-09 11:40:59', 13),
(40, 'Login', '2014-09-09 11:41:03', 14),
(41, 'Login', '2014-09-10 09:30:15', 2),
(42, 'logout', '2014-09-10 09:30:19', 2),
(43, 'Login', '2014-09-10 09:30:33', 2),
(44, 'logout', '2014-09-10 09:51:14', 2),
(45, 'Login', '2014-09-10 09:51:38', 2),
(46, 'logout', '2014-09-10 09:51:59', 2),
(47, 'Login', '2014-09-10 09:52:03', 16),
(48, 'logout', '2014-09-10 09:53:33', 16),
(49, 'Login', '2014-09-10 09:53:38', 16),
(50, 'logout', '2014-09-10 09:53:42', 16),
(51, 'Login', '2014-09-10 09:53:48', 16),
(52, 'logout', '2014-09-10 09:53:50', 16),
(53, 'Login', '2014-09-10 09:53:53', 14),
(54, 'logout', '2014-09-10 09:54:10', 14),
(55, 'Login', '2014-09-10 09:54:14', 16),
(56, 'logout', '2014-09-10 13:54:54', 16),
(57, 'Login', '2014-09-10 13:55:12', 14),
(58, 'logout', '2014-09-10 13:55:17', 14),
(59, 'Login', '2014-09-10 13:55:22', 16),
(60, 'logout', '2014-09-10 13:55:36', 16),
(61, 'Login', '2014-09-10 13:55:40', 16),
(62, 'logout', '2014-09-10 13:55:47', 16),
(63, 'Login', '2014-09-10 13:55:52', 14),
(64, 'Login', '2014-09-11 10:36:37', 14),
(65, 'logout', '2014-09-11 10:43:55', 14),
(66, 'Login', '2014-09-11 10:44:01', 2),
(67, 'logout', '2014-09-11 15:17:04', 2),
(68, 'Login', '2014-09-11 15:17:09', 14),
(69, 'logout', '2014-09-11 15:17:31', 14),
(70, 'Login', '2014-09-11 15:17:36', 17),
(71, 'logout', '2014-09-11 15:17:39', 17),
(72, 'Login', '2014-09-11 15:17:55', 14),
(73, 'Login', '2014-09-12 08:28:26', 2),
(74, 'Login', '2014-09-13 12:39:24', 2),
(75, 'Login', '2014-09-13 14:57:20', 2),
(76, 'Login', '2014-09-13 17:21:48', 2),
(77, 'Login', '2014-09-13 17:34:52', 2),
(78, 'Login', '2014-09-14 12:24:55', 14),
(79, 'logout', '2014-09-14 12:24:59', 14),
(80, 'Login', '2014-09-14 12:25:09', 2),
(81, 'Login', '2014-09-14 20:56:47', 2),
(82, 'Login', '2014-09-15 11:51:37', 14),
(83, 'Login', '2014-09-15 11:54:39', 14),
(84, 'logout', '2014-09-15 14:31:37', 14),
(85, 'Login', '2014-09-15 14:33:16', 17),
(86, 'logout', '2014-09-15 14:33:25', 17),
(87, 'Login', '2014-09-15 14:34:53', 17),
(88, 'logout', '2014-09-15 14:35:11', 17),
(89, 'Login', '2014-09-15 14:35:29', 14),
(90, 'Login', '2014-09-16 05:47:05', 2),
(91, 'Login', '2014-09-16 06:58:52', 2),
(92, 'logout', '2014-09-16 07:47:16', 2),
(93, 'Login', '2014-09-16 08:54:24', 2),
(94, 'logout', '2014-09-16 08:55:42', 2),
(95, 'Login', '2014-09-16 09:45:05', 14),
(96, 'Login', '2014-09-16 09:48:11', 14);

-- --------------------------------------------------------

--
-- Table structure for table `masuk_detail`
--

CREATE TABLE IF NOT EXISTS `masuk_detail` (
  `id_masuk_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_masuk_header` int(11) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(60) NOT NULL,
  `jumlah` float NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `nilai` float NOT NULL,
  PRIMARY KEY (`id_masuk_detail`),
  KEY `FK_masuk_detail_masuk_header` (`id_masuk_header`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `masuk_detail`
--

INSERT INTO `masuk_detail` (`id_masuk_detail`, `id_masuk_header`, `kode_barang`, `nama_barang`, `jumlah`, `satuan`, `nilai`) VALUES
(52, 53, 'BARANG01', 'Barang 01', 5, 'KG', 125000000),
(53, 53, 'BARANG02', 'Barang 02', 10, 'LTR', 75000000),
(54, 53, 'BARANG03', 'Barang 03', 20, 'LTR', 175000000),
(55, 54, 'TEST01', 'Test01', 1, 'KG', 100000),
(56, 54, 'TEST02', 'TEST 02', 20, 'LTR', 70000000);

-- --------------------------------------------------------

--
-- Table structure for table `masuk_header`
--

CREATE TABLE IF NOT EXISTS `masuk_header` (
  `id_masuk_header` int(11) NOT NULL AUTO_INCREMENT,
  `dp_jenis` varchar(10) NOT NULL,
  `dp_nomor` varchar(30) NOT NULL,
  `dp_tanggal` date NOT NULL,
  `bpb_nomor` varchar(30) NOT NULL,
  `bpb_tanggal` date NOT NULL,
  `pengirim` varchar(100) NOT NULL,
  PRIMARY KEY (`id_masuk_header`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `masuk_header`
--

INSERT INTO `masuk_header` (`id_masuk_header`, `dp_jenis`, `dp_nomor`, `dp_tanggal`, `bpb_nomor`, `bpb_tanggal`, `pengirim`) VALUES
(51, 'BC40', '00001/TEST/201409', '2014-09-05', '000124', '2014-09-07', 'PT. ABC'),
(52, 'BC40', '00002/TEST/201409', '2014-09-06', '000125', '2014-09-10', 'PT. XYZ'),
(53, 'BC40', '00001/TEST/201409', '2014-09-05', '000124', '2014-09-07', 'PT. ABC'),
(54, 'BC40', '00002/TEST/201409', '2014-09-06', '000125', '2014-09-10', 'PT. XYZ');

-- --------------------------------------------------------

--
-- Table structure for table `report_pemasukan_bea`
--

CREATE TABLE IF NOT EXISTS `report_pemasukan_bea` (
  `lap_masuk_id` int(11) NOT NULL,
  `dp_jenis` varchar(10) NOT NULL,
  `dp_no` int(11) NOT NULL,
  `dp_tanggal` date NOT NULL,
  `bpb_no` int(11) NOT NULL,
  `bpb_tanggal` date NOT NULL,
  `pengirim_barang` longtext NOT NULL,
  `kode_barang` longtext NOT NULL,
  `nama_barang` longtext NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `nilai` float NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`lap_masuk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `report_pemasukan_bea`
--

INSERT INTO `report_pemasukan_bea` (`lap_masuk_id`, `dp_jenis`, `dp_no`, `dp_tanggal`, `bpb_no`, `bpb_tanggal`, `pengirim_barang`, `kode_barang`, `nama_barang`, `jumlah`, `satuan`, `nilai`, `id_user`) VALUES
(1, 'sfd', 1, '2014-09-09', 1, '2014-09-09', 'saf', 'asdf', 'sda', 12, 'gs', 45, 2),
(2, 'sdf', 2, '2014-09-10', 2, '2014-09-10', 'asdf', 'sdf', 'sad', 5, 'sdf', 42, 2);

-- --------------------------------------------------------

--
-- Table structure for table `report_pengeluaran_bea`
--

CREATE TABLE IF NOT EXISTS `report_pengeluaran_bea` (
  `lap_masuk_id` int(11) NOT NULL,
  `dp_jenis` varchar(10) NOT NULL,
  `dp_no` int(11) NOT NULL,
  `dp_tanggal` date NOT NULL,
  `bpb_no` int(11) NOT NULL,
  `bpb_tanggal` date NOT NULL,
  `pengirim_barang` longtext NOT NULL,
  `kode_barang` longtext NOT NULL,
  `nama_barang` longtext NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `nilai` float NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`lap_masuk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(2) NOT NULL,
  `role_name` varchar(30) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`) VALUES
(1, 'Internal'),
(2, 'BC');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(1) NOT NULL,
  `status_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trandetail`
--

CREATE TABLE IF NOT EXISTS `trandetail` (
  `trandetail_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` mediumtext NOT NULL,
  `quantity` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `tranheader_id` int(11) NOT NULL,
  PRIMARY KEY (`trandetail_id`),
  KEY `FK_trandetail_tranheader` (`tranheader_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `trandetail`
--

INSERT INTO `trandetail` (`trandetail_id`, `kode_barang`, `nama_barang`, `quantity`, `satuan`, `tranheader_id`) VALUES
(70, 'A222', 'bedak', 4, 'METER', 68),
(71, 'A234', 'ahsflah', 5, 'KG', 68),
(72, 'A221', 'aasdas', 9, 'BUAH', 71),
(73, 'A333', 'Barang4', 8, 'PCS', 71),
(74, 'A234', 'ahsflah', 4, 'KG', 72),
(75, 'A221', 'aasdas', 8, 'BUAH', 72),
(76, 'A333', 'Barang4', 9090, 'PCS', 73),
(77, 'A234', 'ahsflah', 9, 'KG', 74),
(78, 'A221', 'aasdas', 9, 'BUAH', 75),
(79, 'A333', 'Barang4', 60, 'PCS', 75),
(80, 'A221', 'aasdas', 9, 'BUAH', 76),
(81, 'A221', 'aasdas', 7, 'BUAH', 76),
(84, 'A221', 'aasdas', 8, 'BUAH', 77),
(85, 'A234', 'ahsflah', 5, 'KG', 78);

-- --------------------------------------------------------

--
-- Table structure for table `tranheader`
--

CREATE TABLE IF NOT EXISTS `tranheader` (
  `tranheader_id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_tran` date NOT NULL,
  `type_tran` char(1) NOT NULL,
  `status_tran` char(1) NOT NULL,
  `user_id` int(3) NOT NULL,
  PRIMARY KEY (`tranheader_id`),
  KEY `FK_tranheader_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `tranheader`
--

INSERT INTO `tranheader` (`tranheader_id`, `tanggal_tran`, `type_tran`, `status_tran`, `user_id`) VALUES
(68, '2014-09-11', 'A', 'P', 2),
(69, '2014-09-11', 'A', 'P', 2),
(70, '2014-09-11', 'P', 'P', 2),
(71, '2014-09-11', 'A', 'P', 2),
(72, '2014-09-25', 'A', 'P', 2),
(73, '2014-09-10', 'S', 'P', 2),
(74, '2014-09-11', 'A', 'P', 2),
(75, '2014-09-11', 'A', 'P', 14),
(76, '2014-09-11', 'K', 'P', 14),
(77, '2014-09-18', 'K', 'P', 14),
(78, '2014-09-06', 'S', 'P', 14),
(79, '2014-09-12', 'P', 'N', 14);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(60) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `saltPassword` varchar(30) NOT NULL DEFAULT '0',
  `role` int(2) NOT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_user_role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `fullname`, `user_name`, `password`, `saltPassword`, `role`, `status`) VALUES
(2, 'abdurrasyid', 'ocit2', 'ffb45b3f0584c2aebd08be82cdbd64fc', '540e84b8bdbb81.90213389', 1, 'A'),
(7, 'setia', 'fandi', '2ac54735f7850e3e2090f0a9fc5c872f', '540d28babb22e1.99689431', 2, 'A'),
(8, 'fandi sastra', 'fandi2', 'd2053ef6cfebe836bc275c9d9bcefa3a', '540d28c16eb246.06040463', 2, 'A'),
(9, 'fandi', 'fandi22', 'd7f8865af68c1e58d048e8b678953d43', '540d28c68e40d9.36079408', 2, 'A'),
(10, 'fandi', 'fandi121', '3d0a0df16ea9b1ea3225f0d35dcd6d1b', '540d28cb48c875.36589018', 2, 'A'),
(11, 'fandi', 'fnadi', '472b32b60ebbae4a061f780dafb417f2', '540d28d07e9b71.83861868', 2, 'A'),
(12, 'abdurrasyid hasim', 'ocit3', '2dd57de10110b9d4c81661caf6bcb65c', '540fba1c0780f9.52377848', 2, 'I'),
(13, 'abdurrasyid', 'ocit4', 'fb589b027cd3b082f5027101910d591d', '540e8411569c99.65764032', 1, 'A'),
(14, 'admin', 'admin', 'bdcd2232656d62ac1308cef65b873c0a', '540e84d8c61185.75092730', 1, 'A'),
(16, 'abdurrasyid hasim', 'ocit8', '844d6159a3f8823fb75d13ba066216e5', '540fbcc8a53435.77537035', 2, 'A'),
(17, 'bc', 'bc', '7616b64975ba3ab57fe9950d71f54c6f', '54115a99d814a4.61784126', 2, 'A');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inventorymaster`
--
ALTER TABLE `inventorymaster`
  ADD CONSTRAINT `FK_inventorymaster_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `keluar_detail`
--
ALTER TABLE `keluar_detail`
  ADD CONSTRAINT `FK_keluar_detail_keluar_header` FOREIGN KEY (`id_keluar_header`) REFERENCES `keluar_header` (`id_keluar_header`);

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_log_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `masuk_detail`
--
ALTER TABLE `masuk_detail`
  ADD CONSTRAINT `FK_masuk_detail_masuk_header` FOREIGN KEY (`id_masuk_header`) REFERENCES `masuk_header` (`id_masuk_header`);

--
-- Constraints for table `trandetail`
--
ALTER TABLE `trandetail`
  ADD CONSTRAINT `FK_trandetail_tranheader` FOREIGN KEY (`tranheader_id`) REFERENCES `tranheader` (`tranheader_id`);

--
-- Constraints for table `tranheader`
--
ALTER TABLE `tranheader`
  ADD CONSTRAINT `FK_tranheader_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`role`) REFERENCES `role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
